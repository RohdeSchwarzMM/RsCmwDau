from typing import List

from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup
from .......Internal.ArgSingleSuppressed import ArgSingleSuppressed
from .......Internal.Types import DataType


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class Current:
	"""Current commands group definition. 2 total commands, 0 Sub-groups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._base = CommandsGroup("current", core, parent)

	def fetch(self) -> List[float]:
		"""SCPI: FETCh:DATA:MEASurement<Instance>:ADELay:TRACe:DLINk[:CURRent] \n
		Snippet: value: List[float] = driver.data.measurement.adelay.trace.dlink.current.fetch() \n
		Query the values of the audio delay traces 'Uplink', 'Downlink' and 'Loopback'. The trace values are returned from right
		to left, from sample number 0 to sample number -N. N equals the configured maximum number of samples minus one, see
		method RsCmwDau.Configure.Data.Measurement.Adelay.msamples. \n
		Use RsCmwDau.reliability.last_value to read the updated reliability indicator. \n
			:return: results: Comma-separated list of delay values, one result per sample Unit: s"""
		suppressed = ArgSingleSuppressed(0, DataType.Integer, False, 1, 'Reliability')
		response = self._core.io.query_bin_or_ascii_float_list_suppressed(f'FETCh:DATA:MEASurement<MeasInstance>:ADELay:TRACe:DLINk:CURRent?', suppressed)
		return response

	def read(self) -> List[float]:
		"""SCPI: READ:DATA:MEASurement<Instance>:ADELay:TRACe:DLINk[:CURRent] \n
		Snippet: value: List[float] = driver.data.measurement.adelay.trace.dlink.current.read() \n
		Query the values of the audio delay traces 'Uplink', 'Downlink' and 'Loopback'. The trace values are returned from right
		to left, from sample number 0 to sample number -N. N equals the configured maximum number of samples minus one, see
		method RsCmwDau.Configure.Data.Measurement.Adelay.msamples. \n
		Use RsCmwDau.reliability.last_value to read the updated reliability indicator. \n
			:return: results: Comma-separated list of delay values, one result per sample Unit: s"""
		suppressed = ArgSingleSuppressed(0, DataType.Integer, False, 1, 'Reliability')
		response = self._core.io.query_bin_or_ascii_float_list_suppressed(f'READ:DATA:MEASurement<MeasInstance>:ADELay:TRACe:DLINk:CURRent?', suppressed)
		return response

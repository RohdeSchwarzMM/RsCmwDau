from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class Kyword:
	"""Kyword commands group definition. 3 total commands, 1 Sub-groups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._base = CommandsGroup("kyword", core, parent)

	@property
	def search(self):
		"""search commands group. 0 Sub-classes, 3 commands."""
		if not hasattr(self, '_search'):
			from .Kyword_.Search import Search
			self._search = Search(self._core, self._base)
		return self._search

	def clone(self) -> 'Kyword':
		"""Clones the group by creating new object from it and its whole existing sub-groups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = Kyword(self._core, self._base.parent)
		self._base.synchronize_repcaps(new_group)
		return new_group

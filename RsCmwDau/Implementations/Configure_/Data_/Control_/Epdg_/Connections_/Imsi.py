from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup
from .......Internal.RepeatedCapability import RepeatedCapability
from ....... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class Imsi:
	"""Imsi commands group definition. 1 total commands, 1 Sub-groups, 0 group commands
	Repeated Capability: Imsi, default value after init: Imsi.Ix1"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._base = CommandsGroup("imsi", core, parent)
		self._base.rep_cap = RepeatedCapability(self._base.group_name, 'repcap_imsi_get', 'repcap_imsi_set', repcap.Imsi.Ix1)

	def repcap_imsi_set(self, enum_value: repcap.Imsi) -> None:
		"""Repeated Capability default value numeric suffix.
		This value is used, if you do not explicitely set it in the child set/get methods, or if you leave it to Imsi.Default
		Default value after init: Imsi.Ix1"""
		self._base.set_repcap_enum_value(enum_value)

	def repcap_imsi_get(self) -> repcap.Imsi:
		"""Returns the current default repeated capability for the child set/get methods"""
		# noinspection PyTypeChecker
		return self._base.get_repcap_enum_value()

	@property
	def apn(self):
		"""apn commands group. 1 Sub-classes, 0 commands."""
		if not hasattr(self, '_apn'):
			from .Imsi_.Apn import Apn
			self._apn = Apn(self._core, self._base)
		return self._apn

	def clone(self) -> 'Imsi':
		"""Clones the group by creating new object from it and its whole existing sub-groups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = Imsi(self._core, self._base.parent)
		self._base.synchronize_repcaps(new_group)
		return new_group

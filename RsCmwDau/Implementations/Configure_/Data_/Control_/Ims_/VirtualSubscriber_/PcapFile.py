from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class PcapFile:
	"""PcapFile commands group definition. 2 total commands, 2 Sub-groups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._base = CommandsGroup("pcapFile", core, parent)

	@property
	def streaming(self):
		"""streaming commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_streaming'):
			from .PcapFile_.Streaming import Streaming
			self._streaming = Streaming(self._core, self._base)
		return self._streaming

	@property
	def selection(self):
		"""selection commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_selection'):
			from .PcapFile_.Selection import Selection
			self._selection = Selection(self._core, self._base)
		return self._selection

	def clone(self) -> 'PcapFile':
		"""Clones the group by creating new object from it and its whole existing sub-groups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = PcapFile(self._core, self._base.parent)
		self._base.synchronize_repcaps(new_group)
		return new_group

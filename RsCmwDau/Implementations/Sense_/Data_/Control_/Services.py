from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal.Utilities import trim_str_response


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class Services:
	"""Services commands group definition. 1 total commands, 0 Sub-groups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._base = CommandsGroup("services", core, parent)

	def get_version(self) -> str:
		"""SCPI: SENSe:DATA:CONTrol:SERVices:VERSion \n
		Snippet: value: str = driver.sense.data.control.services.get_version() \n
		No command help available \n
			:return: version_list: No help available
		"""
		response = self._core.io.query_str('SENSe:DATA:CONTrol:SERVices:VERSion?')
		return trim_str_response(response)

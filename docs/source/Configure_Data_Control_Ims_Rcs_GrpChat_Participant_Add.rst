Add
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:RCS:GRPChat:PARTicipant:ADD

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:RCS:GRPChat:PARTicipant:ADD



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Rcs_.GrpChat_.Participant_.Add.Add
	:members:
	:undoc-members:
	:noindex:
Adelay
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:DATA:MEASurement<MeasInstance>:ADELay
	single: STOP:DATA:MEASurement<MeasInstance>:ADELay
	single: ABORt:DATA:MEASurement<MeasInstance>:ADELay

.. code-block:: python

	INITiate:DATA:MEASurement<MeasInstance>:ADELay
	STOP:DATA:MEASurement<MeasInstance>:ADELay
	ABORt:DATA:MEASurement<MeasInstance>:ADELay



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Adelay.Adelay
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.adelay.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_Adelay_State.rst
	Data_Measurement_Adelay_Ulink.rst
	Data_Measurement_Adelay_Dlink.rst
	Data_Measurement_Adelay_Loopback.rst
	Data_Measurement_Adelay_TauLink.rst
	Data_Measurement_Adelay_TaLoopback.rst
	Data_Measurement_Adelay_Trace.rst
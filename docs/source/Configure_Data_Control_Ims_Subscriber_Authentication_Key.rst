Key
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:SUBScriber<Subscriber>:AUTHenticati:KEY

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:SUBScriber<Subscriber>:AUTHenticati:KEY



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Subscriber_.Authentication_.Key.Key
	:members:
	:undoc-members:
	:noindex:
Dlink<Dlink>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix4
	rc = driver.data.measurement.throughput.ran.dlink.repcap_dlink_get()
	driver.data.measurement.throughput.ran.dlink.repcap_dlink_set(repcap.Dlink.Ix1)



.. rubric:: SCPI Commands

.. index::
	single: READ:DATA:MEASurement<MeasInstance>:THRoughput:RAN:DLINk<Dlink>
	single: FETCh:DATA:MEASurement<MeasInstance>:THRoughput:RAN:DLINk<Dlink>

.. code-block:: python

	READ:DATA:MEASurement<MeasInstance>:THRoughput:RAN:DLINk<Dlink>
	FETCh:DATA:MEASurement<MeasInstance>:THRoughput:RAN:DLINk<Dlink>



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Ran_.Dlink.Dlink
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.throughput.ran.dlink.clone()
Add
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:CONFerence:FACTory:ADD

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:CONFerence:FACTory:ADD



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Conference_.Factory_.Add.Add
	:members:
	:undoc-members:
	:noindex:
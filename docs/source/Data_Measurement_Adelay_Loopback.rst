Loopback
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:ADELay:LOOPback
	single: READ:DATA:MEASurement<MeasInstance>:ADELay:LOOPback

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:ADELay:LOOPback
	READ:DATA:MEASurement<MeasInstance>:ADELay:LOOPback



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Adelay_.Loopback.Loopback
	:members:
	:undoc-members:
	:noindex:
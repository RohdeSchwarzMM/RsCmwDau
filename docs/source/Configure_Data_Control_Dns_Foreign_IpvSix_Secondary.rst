Secondary
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:DNS:FOReign:IPVSix:SECondary:UDHCp
	single: CONFigure:DATA:CONTrol:DNS:FOReign:IPVSix:SECondary:ADDRess

.. code-block:: python

	CONFigure:DATA:CONTrol:DNS:FOReign:IPVSix:SECondary:UDHCp
	CONFigure:DATA:CONTrol:DNS:FOReign:IPVSix:SECondary:ADDRess



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Dns_.Foreign_.IpvSix_.Secondary.Secondary
	:members:
	:undoc-members:
	:noindex:
Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:ADELay:TRACe:DLINk:CURRent
	single: READ:DATA:MEASurement<MeasInstance>:ADELay:TRACe:DLINk:CURRent

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:ADELay:TRACe:DLINk:CURRent
	READ:DATA:MEASurement<MeasInstance>:ADELay:TRACe:DLINk:CURRent



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Adelay_.Trace_.Dlink_.Current.Current
	:members:
	:undoc-members:
	:noindex:
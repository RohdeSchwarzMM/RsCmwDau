Amf
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:SUBScriber<Subscriber>:AUTHenticati:AMF

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:SUBScriber<Subscriber>:AUTHenticati:AMF



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Subscriber_.Authentication_.Amf.Amf
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:DATA:CONTrol:EPDG:STATe

.. code-block:: python

	SOURce:DATA:CONTrol:EPDG:STATe



.. autoclass:: RsCmwDau.Implementations.Source_.Data_.Control_.Epdg_.State.State
	:members:
	:undoc-members:
	:noindex:
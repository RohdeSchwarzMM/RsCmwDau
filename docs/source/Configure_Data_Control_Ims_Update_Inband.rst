Inband
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Inband.Inband
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.update.inband.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_Update_Inband_Perform.rst
	Configure_Data_Control_Ims_Update_Inband_Evs.rst
	Configure_Data_Control_Ims_Update_Inband_Repetition.rst
	Configure_Data_Control_Ims_Update_Inband_AmRnb.rst
	Configure_Data_Control_Ims_Update_Inband_AmRwb.rst
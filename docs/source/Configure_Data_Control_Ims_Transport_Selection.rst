Selection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:TRANsport:SELection

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:TRANsport:SELection



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Transport_.Selection.Selection
	:members:
	:undoc-members:
	:noindex:
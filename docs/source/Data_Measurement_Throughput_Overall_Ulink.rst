Ulink
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:THRoughput:OVERall:ULINk
	single: READ:DATA:MEASurement<MeasInstance>:THRoughput:OVERall:ULINk

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:THRoughput:OVERall:ULINk
	READ:DATA:MEASurement<MeasInstance>:THRoughput:OVERall:ULINk



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Overall_.Ulink.Ulink
	:members:
	:undoc-members:
	:noindex:
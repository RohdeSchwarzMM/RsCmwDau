Trigger
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.FtTrigger_.Trigger.Trigger
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.ipAnalysis.ftTrigger.trigger.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_IpAnalysis_FtTrigger_Trigger_Start.rst
	Data_Measurement_IpAnalysis_FtTrigger_Trigger_End.rst
Drate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:NIMPairments<Impairments>:DRATe

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:NIMPairments<Impairments>:DRATe



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Nimpairments_.Drate.Drate
	:members:
	:undoc-members:
	:noindex:
Alignment
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:AMR:ALIGnment

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:AMR:ALIGnment



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Amr_.Alignment.Alignment
	:members:
	:undoc-members:
	:noindex:
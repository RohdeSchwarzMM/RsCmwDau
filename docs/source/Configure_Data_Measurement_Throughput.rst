Throughput
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:THRoughput:MCOunt

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:THRoughput:MCOunt



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Throughput.Throughput
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.throughput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Measurement_Throughput_Ran.rst
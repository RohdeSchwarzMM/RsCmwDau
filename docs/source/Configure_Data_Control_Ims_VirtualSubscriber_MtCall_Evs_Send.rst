Send
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.MtCall_.Evs_.Send.Send
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.virtualSubscriber.mtCall.evs.send.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_VirtualSubscriber_MtCall_Evs_Send_Bw.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtCall_Evs_Send_Bitrate.rst
Search
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:KYWord:SEARch:IMPort
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:KYWord:SEARch:ADD
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:KYWord:SEARch:DELete

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:KYWord:SEARch:IMPort
	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:KYWord:SEARch:ADD
	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:KYWord:SEARch:DELete



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.IpAnalysis_.IpcSecurity_.Kyword_.Search.Search
	:members:
	:undoc-members:
	:noindex:
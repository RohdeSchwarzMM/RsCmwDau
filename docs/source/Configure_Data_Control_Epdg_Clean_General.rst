General
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Clean_.General.General
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.epdg.clean.general.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Epdg_Clean_General_Info.rst
FilterPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:IPConnect:FILTer:EXTension
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:IPConnect:FILTer

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:IPConnect:FILTer:EXTension
	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:IPConnect:FILTer



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.IpAnalysis_.IpConnect_.FilterPy.FilterPy
	:members:
	:undoc-members:
	:noindex:
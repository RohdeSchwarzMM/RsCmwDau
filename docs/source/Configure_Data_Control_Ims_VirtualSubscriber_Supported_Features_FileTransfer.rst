FileTransfer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:SUPPorted:FEATures:FILetransfer

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:SUPPorted:FEATures:FILetransfer



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.Supported_.Features_.FileTransfer.FileTransfer
	:members:
	:undoc-members:
	:noindex:
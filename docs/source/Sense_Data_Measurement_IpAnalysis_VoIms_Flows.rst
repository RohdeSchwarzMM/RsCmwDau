Flows
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:VOIMs:FLOWs

.. code-block:: python

	SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:VOIMs:FLOWs



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Measurement_.IpAnalysis_.VoIms_.Flows.Flows
	:members:
	:undoc-members:
	:noindex:
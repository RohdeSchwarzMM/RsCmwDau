Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:EVS:CODec<Codec>:ENABle

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:EVS:CODec<Codec>:ENABle



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.Evs_.Codec_.Enable.Enable
	:members:
	:undoc-members:
	:noindex:
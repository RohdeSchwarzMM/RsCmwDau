Bw
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:INBand:EVS:BW

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:INBand:EVS:BW



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Inband_.Evs_.Bw.Bw
	:members:
	:undoc-members:
	:noindex:
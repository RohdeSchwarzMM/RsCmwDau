Addresses
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IPVFour:STATic:ADDResses:ADD
	single: CONFigure:DATA:CONTrol:IPVFour:STATic:ADDResses:DELete

.. code-block:: python

	CONFigure:DATA:CONTrol:IPVFour:STATic:ADDResses:ADD
	CONFigure:DATA:CONTrol:IPVFour:STATic:ADDResses:DELete



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.IpvFour_.Static_.Addresses.Addresses
	:members:
	:undoc-members:
	:noindex:
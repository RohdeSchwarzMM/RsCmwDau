VirtualSubscriber<VirtualSubscriber>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr20
	rc = driver.configure.data.control.ims.virtualSubscriber.repcap_virtualSubscriber_get()
	driver.configure.data.control.ims.virtualSubscriber.repcap_virtualSubscriber_set(repcap.VirtualSubscriber.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:DELete

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:DELete



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber.VirtualSubscriber
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.virtualSubscriber.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_VirtualSubscriber_EcConfig.rst
	Configure_Data_Control_Ims_VirtualSubscriber_FwdCall.rst
	Configure_Data_Control_Ims_VirtualSubscriber_Session.rst
	Configure_Data_Control_Ims_VirtualSubscriber_Evs.rst
	Configure_Data_Control_Ims_VirtualSubscriber_Conference.rst
	Configure_Data_Control_Ims_VirtualSubscriber_Supported.rst
	Configure_Data_Control_Ims_VirtualSubscriber_PcapFile.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtFileTfr.rst
	Configure_Data_Control_Ims_VirtualSubscriber_AudioBoard.rst
	Configure_Data_Control_Ims_VirtualSubscriber_ForceMoCall.rst
	Configure_Data_Control_Ims_VirtualSubscriber_Bearer.rst
	Configure_Data_Control_Ims_VirtualSubscriber_Id.rst
	Configure_Data_Control_Ims_VirtualSubscriber_Behaviour.rst
	Configure_Data_Control_Ims_VirtualSubscriber_SignalingType.rst
	Configure_Data_Control_Ims_VirtualSubscriber_AdCodec.rst
	Configure_Data_Control_Ims_VirtualSubscriber_Amr.rst
	Configure_Data_Control_Ims_VirtualSubscriber_Video.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MediaEndpoint.rst
	Configure_Data_Control_Ims_VirtualSubscriber_Forward.rst
	Configure_Data_Control_Ims_VirtualSubscriber_Add.rst
	Configure_Data_Control_Ims_VirtualSubscriber_Create.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtSms.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtCall.rst
	Configure_Data_Control_Ims_VirtualSubscriber_Max.rst
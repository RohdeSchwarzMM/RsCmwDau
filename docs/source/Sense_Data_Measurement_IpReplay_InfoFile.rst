InfoFile
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:MEASurement<MeasInstance>:IPReplay:INFofile

.. code-block:: python

	SENSe:DATA:MEASurement<MeasInstance>:IPReplay:INFofile



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Measurement_.IpReplay_.InfoFile.InfoFile
	:members:
	:undoc-members:
	:noindex:
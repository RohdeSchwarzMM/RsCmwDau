Create
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:SUBScriber:CREate

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:SUBScriber:CREate



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Subscriber_.Create.Create
	:members:
	:undoc-members:
	:noindex:
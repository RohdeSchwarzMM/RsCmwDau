Ike
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:IKE:ENCRyption
	single: CONFigure:DATA:CONTrol:EPDG:IKE:PRF
	single: CONFigure:DATA:CONTrol:EPDG:IKE:INTegrity
	single: CONFigure:DATA:CONTrol:EPDG:IKE:DHGRoup
	single: CONFigure:DATA:CONTrol:EPDG:IKE:LIFetime

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:IKE:ENCRyption
	CONFigure:DATA:CONTrol:EPDG:IKE:PRF
	CONFigure:DATA:CONTrol:EPDG:IKE:INTegrity
	CONFigure:DATA:CONTrol:EPDG:IKE:DHGRoup
	CONFigure:DATA:CONTrol:EPDG:IKE:LIFetime



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Ike.Ike
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.epdg.ike.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Epdg_Ike_Rekey.rst
Protocol
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:CLIent<Client>:PROTocol

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:CLIent<Client>:PROTocol



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Client_.Protocol.Protocol
	:members:
	:undoc-members:
	:noindex:
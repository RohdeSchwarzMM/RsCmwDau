RegExp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:PCSCf<PcscFnc>:REGexp

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:PCSCf<PcscFnc>:REGexp



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Pcscf_.RegExp.RegExp
	:members:
	:undoc-members:
	:noindex:
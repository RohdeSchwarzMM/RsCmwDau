Apn<AccPointName>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr15
	rc = driver.configure.data.control.epdg.connections.imsi.apn.repcap_accPointName_get()
	driver.configure.data.control.epdg.connections.imsi.apn.repcap_accPointName_set(repcap.AccPointName.Nr1)





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Connections_.Imsi_.Apn.Apn
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.epdg.connections.imsi.apn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Epdg_Connections_Imsi_Apn_Release.rst
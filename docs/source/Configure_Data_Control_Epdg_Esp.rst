Esp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:ESP:ENCRyption
	single: CONFigure:DATA:CONTrol:EPDG:ESP:INTegrity
	single: CONFigure:DATA:CONTrol:EPDG:ESP:LIFetime

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:ESP:ENCRyption
	CONFigure:DATA:CONTrol:EPDG:ESP:INTegrity
	CONFigure:DATA:CONTrol:EPDG:ESP:LIFetime



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Esp.Esp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.epdg.esp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Epdg_Esp_Rekey.rst
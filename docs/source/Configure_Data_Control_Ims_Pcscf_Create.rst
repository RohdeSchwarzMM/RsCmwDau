Create
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:PCSCf:CREate

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:PCSCf:CREate



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Pcscf_.Create.Create
	:members:
	:undoc-members:
	:noindex:
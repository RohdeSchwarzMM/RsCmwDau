Pcscf<PcscFnc>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr10
	rc = driver.configure.data.control.ims.pcscf.repcap_pcscFnc_get()
	driver.configure.data.control.ims.pcscf.repcap_pcscFnc_set(repcap.PcscFnc.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:PCSCf<PcscFnc>:DELete

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:PCSCf<PcscFnc>:DELete



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Pcscf.Pcscf
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.pcscf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_Pcscf_IpAddress.rst
	Configure_Data_Control_Ims_Pcscf_Behaviour.rst
	Configure_Data_Control_Ims_Pcscf_FailureCode.rst
	Configure_Data_Control_Ims_Pcscf_RetryAfter.rst
	Configure_Data_Control_Ims_Pcscf_RegExp.rst
	Configure_Data_Control_Ims_Pcscf_SubExp.rst
	Configure_Data_Control_Ims_Pcscf_Add.rst
	Configure_Data_Control_Ims_Pcscf_Create.rst
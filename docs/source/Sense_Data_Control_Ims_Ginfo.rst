Ginfo
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IMS<Ims>:GINFo

.. code-block:: python

	SENSe:DATA:CONTrol:IMS<Ims>:GINFo



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.Ginfo.Ginfo
	:members:
	:undoc-members:
	:noindex:
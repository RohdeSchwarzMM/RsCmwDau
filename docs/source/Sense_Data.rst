Data
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Sense_.Data.Data
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control.rst
	Sense_Data_Measurement.rst
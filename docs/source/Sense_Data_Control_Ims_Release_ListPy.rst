ListPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IMS<Ims>:RELease:LIST

.. code-block:: python

	SENSe:DATA:CONTrol:IMS<Ims>:RELease:LIST



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.Release_.ListPy.ListPy
	:members:
	:undoc-members:
	:noindex:
Throughput
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:DATA:MEASurement<MeasInstance>:THRoughput
	single: STOP:DATA:MEASurement<MeasInstance>:THRoughput
	single: ABORt:DATA:MEASurement<MeasInstance>:THRoughput

.. code-block:: python

	INITiate:DATA:MEASurement<MeasInstance>:THRoughput
	STOP:DATA:MEASurement<MeasInstance>:THRoughput
	ABORt:DATA:MEASurement<MeasInstance>:THRoughput



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput.Throughput
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.throughput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_Throughput_State.rst
	Data_Measurement_Throughput_Trace.rst
	Data_Measurement_Throughput_Overall.rst
	Data_Measurement_Throughput_Ran.rst
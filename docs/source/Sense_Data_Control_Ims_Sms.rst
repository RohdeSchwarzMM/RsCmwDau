Sms
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IMS:SMS:RECeived

.. code-block:: python

	SENSe:DATA:CONTrol:IMS:SMS:RECeived



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.Sms.Sms
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.ims.sms.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_Ims_Sms_Send.rst
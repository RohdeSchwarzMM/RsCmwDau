Ftp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:FTP:STYPe
	single: CONFigure:DATA:CONTrol:FTP:ENConnection
	single: CONFigure:DATA:CONTrol:FTP:AUSer
	single: CONFigure:DATA:CONTrol:FTP:DUPLoad
	single: CONFigure:DATA:CONTrol:FTP:IPVSix

.. code-block:: python

	CONFigure:DATA:CONTrol:FTP:STYPe
	CONFigure:DATA:CONTrol:FTP:ENConnection
	CONFigure:DATA:CONTrol:FTP:AUSer
	CONFigure:DATA:CONTrol:FTP:DUPLoad
	CONFigure:DATA:CONTrol:FTP:IPVSix



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ftp.Ftp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ftp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ftp_User.rst
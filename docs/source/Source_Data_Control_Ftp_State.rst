State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:DATA:CONTrol:FTP:STATe

.. code-block:: python

	SOURce:DATA:CONTrol:FTP:STATe



.. autoclass:: RsCmwDau.Implementations.Source_.Data_.Control_.Ftp_.State.State
	:members:
	:undoc-members:
	:noindex:
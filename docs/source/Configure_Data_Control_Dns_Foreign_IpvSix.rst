IpvSix
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Dns_.Foreign_.IpvSix.IpvSix
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.dns.foreign.ipvSix.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Dns_Foreign_IpvSix_Primary.rst
	Configure_Data_Control_Dns_Foreign_IpvSix_Secondary.rst
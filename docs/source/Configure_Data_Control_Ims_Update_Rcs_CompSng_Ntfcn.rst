Ntfcn
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:RCS:COMPsng:NTFCn

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:RCS:COMPsng:NTFCn



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Rcs_.CompSng_.Ntfcn.Ntfcn
	:members:
	:undoc-members:
	:noindex:
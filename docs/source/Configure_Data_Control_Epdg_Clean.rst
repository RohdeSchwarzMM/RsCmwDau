Clean
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Clean.Clean
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.epdg.clean.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Epdg_Clean_General.rst
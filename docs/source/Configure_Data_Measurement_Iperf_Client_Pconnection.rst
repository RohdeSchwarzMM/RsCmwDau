Pconnection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:CLIent<Client>:PCONnection

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:CLIent<Client>:PCONnection



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Client_.Pconnection.Pconnection
	:members:
	:undoc-members:
	:noindex:
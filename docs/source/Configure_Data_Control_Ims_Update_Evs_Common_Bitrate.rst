Bitrate
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Evs_.Common_.Bitrate.Bitrate
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.update.evs.common.bitrate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_Update_Evs_Common_Bitrate_Range.rst
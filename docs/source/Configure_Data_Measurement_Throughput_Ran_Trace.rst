Trace
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Throughput_.Ran_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.throughput.ran.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Measurement_Throughput_Ran_Trace_Dlink.rst
	Configure_Data_Measurement_Throughput_Ran_Trace_Ulink.rst
Call
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IMS:VOICe:CALL:STATe

.. code-block:: python

	SENSe:DATA:CONTrol:IMS:VOICe:CALL:STATe



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.Voice_.Call.Call
	:members:
	:undoc-members:
	:noindex:
IpAddress
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:NIMPairments<Impairments>:IPADdress

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:NIMPairments<Impairments>:IPADdress



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Nimpairments_.IpAddress.IpAddress
	:members:
	:undoc-members:
	:noindex:
MendPoint
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS:VOICe:MENDpoint:PORT
	single: CONFigure:DATA:CONTrol:IMS:VOICe:MENDpoint:IPADdress

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS:VOICe:MENDpoint:PORT
	CONFigure:DATA:CONTrol:IMS:VOICe:MENDpoint:IPADdress



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Voice_.MendPoint.MendPoint
	:members:
	:undoc-members:
	:noindex:
Prefixes
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IPVSix:STATic:PREFixes:ADD
	single: CONFigure:DATA:CONTrol:IPVSix:STATic:PREFixes:DELete

.. code-block:: python

	CONFigure:DATA:CONTrol:IPVSix:STATic:PREFixes:ADD
	CONFigure:DATA:CONTrol:IPVSix:STATic:PREFixes:DELete



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.IpvSix_.Static_.Prefixes.Prefixes
	:members:
	:undoc-members:
	:noindex:
Negotiated
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:CAPPlication:HANDshake:NEGotiated

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:CAPPlication:HANDshake:NEGotiated



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.IpcSecurity_.Capplication_.Handshake_.Negotiated.Negotiated
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.ipAnalysis.ipcSecurity.capplication.handshake.negotiated.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_IpAnalysis_IpcSecurity_Capplication_Handshake_Negotiated_EcpFormats.rst
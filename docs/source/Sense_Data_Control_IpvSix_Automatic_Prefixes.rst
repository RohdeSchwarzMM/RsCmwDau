Prefixes
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IPVSix:AUTomatic:PREFixes:CATalog

.. code-block:: python

	SENSe:DATA:CONTrol:IPVSix:AUTomatic:PREFixes:CATalog



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.IpvSix_.Automatic_.Prefixes.Prefixes
	:members:
	:undoc-members:
	:noindex:
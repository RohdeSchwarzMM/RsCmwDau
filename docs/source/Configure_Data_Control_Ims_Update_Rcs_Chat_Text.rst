Text
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:RCS:CHAT:TEXT

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:RCS:CHAT:TEXT



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Rcs_.Chat_.Text.Text
	:members:
	:undoc-members:
	:noindex:
Info
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:CLEan:GENeral:INFO

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:CLEan:GENeral:INFO



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Clean_.General_.Info.Info
	:members:
	:undoc-members:
	:noindex:
Ecall
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.Ecall.Ecall
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.ims.ecall.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_Ims_Ecall_Msd.rst
	Sense_Data_Control_Ims_Ecall_TypePy.rst
	Sense_Data_Control_Ims_Ecall_CallId.rst
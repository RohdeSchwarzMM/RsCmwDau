Info
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:CLEan:GENeral:INFO

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:CLEan:GENeral:INFO



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Clean_.General_.Info.Info
	:members:
	:undoc-members:
	:noindex:
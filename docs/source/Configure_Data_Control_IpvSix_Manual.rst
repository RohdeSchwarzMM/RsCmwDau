Manual
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.IpvSix_.Manual.Manual
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ipvSix.manual.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_IpvSix_Manual_Routing.rst
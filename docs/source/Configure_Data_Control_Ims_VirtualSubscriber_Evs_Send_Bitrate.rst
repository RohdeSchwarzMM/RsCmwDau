Bitrate
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.Evs_.Send_.Bitrate.Bitrate
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.virtualSubscriber.evs.send.bitrate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_VirtualSubscriber_Evs_Send_Bitrate_Range.rst
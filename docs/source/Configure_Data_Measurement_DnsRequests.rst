DnsRequests
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:DNSRequests:MICount

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:DNSRequests:MICount



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.DnsRequests.DnsRequests
	:members:
	:undoc-members:
	:noindex:
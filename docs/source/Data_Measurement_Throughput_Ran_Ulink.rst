Ulink<Slot>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.data.measurement.throughput.ran.ulink.repcap_slot_get()
	driver.data.measurement.throughput.ran.ulink.repcap_slot_set(repcap.Slot.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: READ:DATA:MEASurement<MeasInstance>:THRoughput:RAN:ULINk<Slot>
	single: FETCh:DATA:MEASurement<MeasInstance>:THRoughput:RAN:ULINk<Slot>

.. code-block:: python

	READ:DATA:MEASurement<MeasInstance>:THRoughput:RAN:ULINk<Slot>
	FETCh:DATA:MEASurement<MeasInstance>:THRoughput:RAN:ULINk<Slot>



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Ran_.Ulink.Ulink
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.throughput.ran.ulink.clone()
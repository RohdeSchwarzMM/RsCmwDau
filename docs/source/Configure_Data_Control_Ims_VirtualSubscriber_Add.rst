Add
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub:ADD

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub:ADD



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.Add.Add
	:members:
	:undoc-members:
	:noindex:
Release
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:CONNections:IMSI<Imsi>:APN<AccPointName>:RELease

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:CONNections:IMSI<Imsi>:APN<AccPointName>:RELease



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Connections_.Imsi_.Apn_.Release.Release
	:members:
	:undoc-members:
	:noindex:
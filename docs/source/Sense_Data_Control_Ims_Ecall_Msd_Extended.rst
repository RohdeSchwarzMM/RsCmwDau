Extended
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IMS<Ims>:ECALl:MSD:EXTended

.. code-block:: python

	SENSe:DATA:CONTrol:IMS<Ims>:ECALl:MSD:EXTended



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.Ecall_.Msd_.Extended.Extended
	:members:
	:undoc-members:
	:noindex:
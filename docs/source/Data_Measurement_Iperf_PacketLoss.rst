PacketLoss
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPERf:PACKetloss

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPERf:PACKetloss



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Iperf_.PacketLoss.PacketLoss
	:members:
	:undoc-members:
	:noindex:
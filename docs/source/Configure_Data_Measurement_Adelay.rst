Adelay
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:ADELay:SAMPles
	single: CONFigure:DATA:MEASurement<MeasInstance>:ADELay:SPINterval
	single: CONFigure:DATA:MEASurement<MeasInstance>:ADELay:MSAMples

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:ADELay:SAMPles
	CONFigure:DATA:MEASurement<MeasInstance>:ADELay:SPINterval
	CONFigure:DATA:MEASurement<MeasInstance>:ADELay:MSAMples



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Adelay.Adelay
	:members:
	:undoc-members:
	:noindex:
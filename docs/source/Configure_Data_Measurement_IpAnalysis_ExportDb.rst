ExportDb
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:EXPortdb

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:EXPortdb



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.IpAnalysis_.ExportDb.ExportDb
	:members:
	:undoc-members:
	:noindex:
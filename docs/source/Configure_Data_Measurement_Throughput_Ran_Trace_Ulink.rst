Ulink<Slot>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.configure.data.measurement.throughput.ran.trace.ulink.repcap_slot_get()
	driver.configure.data.measurement.throughput.ran.trace.ulink.repcap_slot_set(repcap.Slot.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:THRoughput:RAN:TRACe:ULINk<Slot>

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:THRoughput:RAN:TRACe:ULINk<Slot>



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Throughput_.Ran_.Trace_.Ulink.Ulink
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.throughput.ran.trace.ulink.clone()
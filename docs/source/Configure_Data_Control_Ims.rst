Ims<Ims>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix2
	rc = driver.configure.data.control.ims.repcap_ims_get()
	driver.configure.data.control.ims.repcap_ims_set(repcap.Ims.Ix1)





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims.Ims
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_VirtualSubscriber.rst
	Configure_Data_Control_Ims_Sip.rst
	Configure_Data_Control_Ims_Rcs.rst
	Configure_Data_Control_Ims_Conference.rst
	Configure_Data_Control_Ims_TcpAlive.rst
	Configure_Data_Control_Ims_Threshold.rst
	Configure_Data_Control_Ims_Transport.rst
	Configure_Data_Control_Ims_Mobile.rst
	Configure_Data_Control_Ims_Susage.rst
	Configure_Data_Control_Ims_Intern.rst
	Configure_Data_Control_Ims_Extern.rst
	Configure_Data_Control_Ims_Uauthentication.rst
	Configure_Data_Control_Ims_Clean.rst
	Configure_Data_Control_Ims_Subscriber.rst
	Configure_Data_Control_Ims_Pcscf.rst
	Configure_Data_Control_Ims_Update.rst
	Configure_Data_Control_Ims_Release.rst
	Configure_Data_Control_Ims_Sms.rst
	Configure_Data_Control_Ims_Voice.rst
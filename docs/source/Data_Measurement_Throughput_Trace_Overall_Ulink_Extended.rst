Extended
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:ULINk:EXTended
	single: FETCh:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:ULINk:EXTended

.. code-block:: python

	READ:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:ULINk:EXTended
	FETCh:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:ULINk:EXTended



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Trace_.Overall_.Ulink_.Extended.Extended
	:members:
	:undoc-members:
	:noindex:
Dlink
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:THRoughput:OVERall:DLINk
	single: READ:DATA:MEASurement<MeasInstance>:THRoughput:OVERall:DLINk

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:THRoughput:OVERall:DLINk
	READ:DATA:MEASurement<MeasInstance>:THRoughput:OVERall:DLINk



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Overall_.Dlink.Dlink
	:members:
	:undoc-members:
	:noindex:
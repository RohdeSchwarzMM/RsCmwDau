IpConnect
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:IPConnect:STATistics

.. code-block:: python

	SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:IPConnect:STATistics



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Measurement_.IpAnalysis_.IpConnect.IpConnect
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.measurement.ipAnalysis.ipConnect.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Measurement_IpAnalysis_IpConnect_AflowId.rst
	Sense_Data_Measurement_IpAnalysis_IpConnect_FlowId.rst
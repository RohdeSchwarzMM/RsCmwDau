Wsize
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:SERVer<Server>:WSIZe

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:SERVer<Server>:WSIZe



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Server_.Wsize.Wsize
	:members:
	:undoc-members:
	:noindex:
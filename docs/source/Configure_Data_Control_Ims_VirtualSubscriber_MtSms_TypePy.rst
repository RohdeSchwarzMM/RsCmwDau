TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:MTSMs:TYPE

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:MTSMs:TYPE



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.MtSms_.TypePy.TypePy
	:members:
	:undoc-members:
	:noindex:
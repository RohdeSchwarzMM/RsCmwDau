Id
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:CALL:ID

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:CALL:ID



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Call_.Id.Id
	:members:
	:undoc-members:
	:noindex:
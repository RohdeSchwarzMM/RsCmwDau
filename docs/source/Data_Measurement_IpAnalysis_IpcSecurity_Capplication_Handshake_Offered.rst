Offered
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.IpcSecurity_.Capplication_.Handshake_.Offered.Offered
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.ipAnalysis.ipcSecurity.capplication.handshake.offered.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_IpAnalysis_IpcSecurity_Capplication_Handshake_Offered_SrIndication.rst
	Data_Measurement_IpAnalysis_IpcSecurity_Capplication_Handshake_Offered_Ecurve.rst
	Data_Measurement_IpAnalysis_IpcSecurity_Capplication_Handshake_Offered_CipSuite.rst
	Data_Measurement_IpAnalysis_IpcSecurity_Capplication_Handshake_Offered_ShAlgorithm.rst
	Data_Measurement_IpAnalysis_IpcSecurity_Capplication_Handshake_Offered_Version.rst
	Data_Measurement_IpAnalysis_IpcSecurity_Capplication_Handshake_Offered_Compression.rst
	Data_Measurement_IpAnalysis_IpcSecurity_Capplication_Handshake_Offered_EcpFormat.rst
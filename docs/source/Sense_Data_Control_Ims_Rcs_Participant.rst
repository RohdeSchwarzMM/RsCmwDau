Participant
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.Rcs_.Participant.Participant
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.ims.rcs.participant.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_Ims_Rcs_Participant_ListPy.rst
Pcscf
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:PCSCf:AUTO

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:PCSCf:AUTO



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Pcscf.Pcscf
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.epdg.pcscf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Epdg_Pcscf_IpvSix.rst
	Configure_Data_Control_Epdg_Pcscf_IpvFour.rst
Prefixes
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IPVSix:STATic:PREFixes:CATalog

.. code-block:: python

	SENSe:DATA:CONTrol:IPVSix:STATic:PREFixes:CATalog



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.IpvSix_.Static_.Prefixes.Prefixes
	:members:
	:undoc-members:
	:noindex:
Data
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data.Data
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control.rst
	Configure_Data_Measurement.rst
Ftp
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ftp.Ftp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.ftp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_Ftp_User.rst
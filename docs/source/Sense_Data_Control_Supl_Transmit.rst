Transmit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:SUPL:TRANsmit:STATus

.. code-block:: python

	SENSe:DATA:CONTrol:SUPL:TRANsmit:STATus



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Supl_.Transmit.Transmit
	:members:
	:undoc-members:
	:noindex:
Secondary
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:DNS:CURRent:IPVFour:SECondary:ADDRess

.. code-block:: python

	SENSe:DATA:CONTrol:DNS:CURRent:IPVFour:SECondary:ADDRess



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Dns_.Current_.IpvFour_.Secondary.Secondary
	:members:
	:undoc-members:
	:noindex:
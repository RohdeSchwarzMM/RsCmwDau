Id
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:ID:TYPE
	single: CONFigure:DATA:CONTrol:EPDG:ID:VALue

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:ID:TYPE
	CONFigure:DATA:CONTrol:EPDG:ID:VALue



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Id.Id
	:members:
	:undoc-members:
	:noindex:
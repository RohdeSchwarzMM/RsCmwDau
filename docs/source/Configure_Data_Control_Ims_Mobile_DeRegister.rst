DeRegister
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:MOBile<Profile>:DERegister

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:MOBile<Profile>:DERegister



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Mobile_.DeRegister.DeRegister
	:members:
	:undoc-members:
	:noindex:
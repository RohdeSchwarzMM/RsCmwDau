Client
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:DATA:MEASurement<MeasInstance>:IPERf:CLIent
	single: FETCh:DATA:MEASurement<MeasInstance>:IPERf:CLIent

.. code-block:: python

	READ:DATA:MEASurement<MeasInstance>:IPERf:CLIent
	FETCh:DATA:MEASurement<MeasInstance>:IPERf:CLIent



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Iperf_.Client.Client
	:members:
	:undoc-members:
	:noindex:
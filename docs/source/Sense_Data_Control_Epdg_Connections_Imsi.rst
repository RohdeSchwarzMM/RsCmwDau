Imsi<Imsi>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix10
	rc = driver.sense.data.control.epdg.connections.imsi.repcap_imsi_get()
	driver.sense.data.control.epdg.connections.imsi.repcap_imsi_set(repcap.Imsi.Ix1)



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:EPDG:CONNections:IMSI

.. code-block:: python

	SENSe:DATA:CONTrol:EPDG:CONNections:IMSI



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Epdg_.Connections_.Imsi.Imsi
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.epdg.connections.imsi.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_Epdg_Connections_Imsi_Apn.rst
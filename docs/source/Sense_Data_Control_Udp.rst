Udp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:UDP:RESult
	single: SENSe:DATA:CONTrol:UDP:RECeive

.. code-block:: python

	SENSe:DATA:CONTrol:UDP:RESult
	SENSe:DATA:CONTrol:UDP:RECeive



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Udp.Udp
	:members:
	:undoc-members:
	:noindex:
PerdTx
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:VOIMs:PERDtx

.. code-block:: python

	SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:VOIMs:PERDtx



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Measurement_.IpAnalysis_.VoIms_.PerdTx.PerdTx
	:members:
	:undoc-members:
	:noindex:
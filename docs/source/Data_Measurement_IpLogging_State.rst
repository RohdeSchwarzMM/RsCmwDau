State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPLogging:STATe

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPLogging:STATe



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpLogging_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.ipLogging.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_IpLogging_State_All.rst
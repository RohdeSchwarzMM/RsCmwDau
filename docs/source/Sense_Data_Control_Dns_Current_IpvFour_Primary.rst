Primary
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:DNS:CURRent:IPVFour:PRIMary:ADDRess

.. code-block:: python

	SENSe:DATA:CONTrol:DNS:CURRent:IPVFour:PRIMary:ADDRess



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Dns_.Current_.IpvFour_.Primary.Primary
	:members:
	:undoc-members:
	:noindex:
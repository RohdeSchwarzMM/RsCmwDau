Interface
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPReplay:INTerface

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPReplay:INTerface



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.IpReplay_.Interface.Interface
	:members:
	:undoc-members:
	:noindex:
PlRate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:NIMPairments<Impairments>:PLRate

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:NIMPairments<Impairments>:PLRate



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Nimpairments_.PlRate.PlRate
	:members:
	:undoc-members:
	:noindex:
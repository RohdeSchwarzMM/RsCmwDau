Ulink
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:THRoughput:RAN:TOTal:SUM:ULINk
	single: READ:DATA:MEASurement<MeasInstance>:THRoughput:RAN:TOTal:SUM:ULINk

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:THRoughput:RAN:TOTal:SUM:ULINk
	READ:DATA:MEASurement<MeasInstance>:THRoughput:RAN:TOTal:SUM:ULINk



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Ran_.Total_.Sum_.Ulink.Ulink
	:members:
	:undoc-members:
	:noindex:
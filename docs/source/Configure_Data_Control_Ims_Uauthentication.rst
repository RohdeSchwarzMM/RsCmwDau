Uauthentication
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS:UAUThentic:PUID
	single: CONFigure:DATA:CONTrol:IMS:UAUThentic:KEY
	single: CONFigure:DATA:CONTrol:IMS:UAUThentic:RAND
	single: CONFigure:DATA:CONTrol:IMS:UAUThentic:ALGorithm
	single: CONFigure:DATA:CONTrol:IMS:UAUThentic:AMF
	single: CONFigure:DATA:CONTrol:IMS:UAUThentic:AKAVersion
	single: CONFigure:DATA:CONTrol:IMS:UAUThentic:KTYPe
	single: CONFigure:DATA:CONTrol:IMS:UAUThentic:AOP
	single: CONFigure:DATA:CONTrol:IMS:UAUThentic:AOPC
	single: CONFigure:DATA:CONTrol:IMS:UAUThentic:RESLength
	single: CONFigure:DATA:CONTrol:IMS:UAUThentic

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS:UAUThentic:PUID
	CONFigure:DATA:CONTrol:IMS:UAUThentic:KEY
	CONFigure:DATA:CONTrol:IMS:UAUThentic:RAND
	CONFigure:DATA:CONTrol:IMS:UAUThentic:ALGorithm
	CONFigure:DATA:CONTrol:IMS:UAUThentic:AMF
	CONFigure:DATA:CONTrol:IMS:UAUThentic:AKAVersion
	CONFigure:DATA:CONTrol:IMS:UAUThentic:KTYPe
	CONFigure:DATA:CONTrol:IMS:UAUThentic:AOP
	CONFigure:DATA:CONTrol:IMS:UAUThentic:AOPC
	CONFigure:DATA:CONTrol:IMS:UAUThentic:RESLength
	CONFigure:DATA:CONTrol:IMS:UAUThentic



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Uauthentication.Uauthentication
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.uauthentication.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_Uauthentication_IpSec.rst
Dlink<Dlink>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix4
	rc = driver.data.measurement.throughput.trace.ran.dlink.repcap_dlink_get()
	driver.data.measurement.throughput.trace.ran.dlink.repcap_dlink_set(repcap.Dlink.Ix1)





.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Trace_.Ran_.Dlink.Dlink
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.throughput.trace.ran.dlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_Throughput_Trace_Ran_Dlink_Current.rst
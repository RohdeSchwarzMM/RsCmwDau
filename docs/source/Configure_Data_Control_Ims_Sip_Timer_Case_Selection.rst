Selection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:SIP:TIMer:CASE:SELection

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:SIP:TIMer:CASE:SELection



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Sip_.Timer_.Case_.Selection.Selection
	:members:
	:undoc-members:
	:noindex:
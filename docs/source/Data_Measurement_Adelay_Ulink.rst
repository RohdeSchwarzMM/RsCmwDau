Ulink
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:ADELay:ULINk
	single: READ:DATA:MEASurement<MeasInstance>:ADELay:ULINk

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:ADELay:ULINk
	READ:DATA:MEASurement<MeasInstance>:ADELay:ULINk



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Adelay_.Ulink.Ulink
	:members:
	:undoc-members:
	:noindex:
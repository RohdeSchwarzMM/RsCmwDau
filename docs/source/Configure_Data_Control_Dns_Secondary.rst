Secondary
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:DNS:SECondary:STYPe

.. code-block:: python

	CONFigure:DATA:CONTrol:DNS:SECondary:STYPe



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Dns_.Secondary.Secondary
	:members:
	:undoc-members:
	:noindex:
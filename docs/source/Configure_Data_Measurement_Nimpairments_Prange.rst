Prange
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:NIMPairments<Impairments>:PRANge

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:NIMPairments<Impairments>:PRANge



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Nimpairments_.Prange.Prange
	:members:
	:undoc-members:
	:noindex:
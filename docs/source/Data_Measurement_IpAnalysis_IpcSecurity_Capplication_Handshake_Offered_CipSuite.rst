CipSuite
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:CAPPlication:HANDshake:OFFered:CIPSuite

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:CAPPlication:HANDshake:OFFered:CIPSuite



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.IpcSecurity_.Capplication_.Handshake_.Offered_.CipSuite.CipSuite
	:members:
	:undoc-members:
	:noindex:
Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:ULINk:CURRent
	single: READ:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:ULINk:CURRent

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:ULINk:CURRent
	READ:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:ULINk:CURRent



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Trace_.Overall_.Ulink_.Current.Current
	:members:
	:undoc-members:
	:noindex:
Trace
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Adelay_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.adelay.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_Adelay_Trace_Ulink.rst
	Data_Measurement_Adelay_Trace_Dlink.rst
	Data_Measurement_Adelay_Trace_Loopback.rst
	Data_Measurement_Adelay_Trace_TauLink.rst
	Data_Measurement_Adelay_Trace_TaLoopback.rst
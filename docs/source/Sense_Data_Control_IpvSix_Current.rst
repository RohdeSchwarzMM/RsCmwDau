Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IPVSix:CURRent:IPADdress
	single: SENSe:DATA:CONTrol:IPVSix:CURRent:DROuter

.. code-block:: python

	SENSe:DATA:CONTrol:IPVSix:CURRent:IPADdress
	SENSe:DATA:CONTrol:IPVSix:CURRent:DROuter



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.IpvSix_.Current.Current
	:members:
	:undoc-members:
	:noindex:
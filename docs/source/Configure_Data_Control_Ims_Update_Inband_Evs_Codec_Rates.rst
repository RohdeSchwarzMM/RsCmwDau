Rates
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:INBand:EVS:CODec:RATes

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:INBand:EVS:CODec:RATes



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Inband_.Evs_.Codec_.Rates.Rates
	:members:
	:undoc-members:
	:noindex:
Dns
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:DNS:RESallquery

.. code-block:: python

	CONFigure:DATA:CONTrol:DNS:RESallquery



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Dns.Dns
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.dns.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Dns_Primary.rst
	Configure_Data_Control_Dns_Secondary.rst
	Configure_Data_Control_Dns_Foreign.rst
	Configure_Data_Control_Dns_Local.rst
	Configure_Data_Control_Dns_Aservices.rst
	Configure_Data_Control_Dns_Test.rst
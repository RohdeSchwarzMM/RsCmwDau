Static
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IPVFour:STATic:GIP
	single: CONFigure:DATA:CONTrol:IPVFour:STATic:IPADdress
	single: CONFigure:DATA:CONTrol:IPVFour:STATic:SMASk

.. code-block:: python

	CONFigure:DATA:CONTrol:IPVFour:STATic:GIP
	CONFigure:DATA:CONTrol:IPVFour:STATic:IPADdress
	CONFigure:DATA:CONTrol:IPVFour:STATic:SMASk



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.IpvFour_.Static.Static
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ipvFour.static.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_IpvFour_Static_Addresses.rst
Apn
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:EPDG:CONNections:IMSI<Imsi>:APN

.. code-block:: python

	SENSe:DATA:CONTrol:EPDG:CONNections:IMSI<Imsi>:APN



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Epdg_.Connections_.Imsi_.Apn.Apn
	:members:
	:undoc-members:
	:noindex:
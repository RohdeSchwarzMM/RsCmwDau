Fthroughput
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:FTTRigger:TRACes<Trace>:FTHRoughput

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:FTTRigger:TRACes<Trace>:FTHRoughput



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.FtTrigger_.Traces_.Fthroughput.Fthroughput
	:members:
	:undoc-members:
	:noindex:
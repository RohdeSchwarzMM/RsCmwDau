JitterDistribution
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:NIMPairments<Impairments>:JDIStribut

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:NIMPairments<Impairments>:JDIStribut



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Nimpairments_.JitterDistribution.JitterDistribution
	:members:
	:undoc-members:
	:noindex:
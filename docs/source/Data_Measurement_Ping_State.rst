State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:PING:STATe

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:PING:STATe



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Ping_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.ping.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_Ping_State_All.rst
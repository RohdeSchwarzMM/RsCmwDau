Dlink
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:THRoughput:RAN:TOTal:SUM:DLINk
	single: READ:DATA:MEASurement<MeasInstance>:THRoughput:RAN:TOTal:SUM:DLINk

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:THRoughput:RAN:TOTal:SUM:DLINk
	READ:DATA:MEASurement<MeasInstance>:THRoughput:RAN:TOTal:SUM:DLINk



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Ran_.Total_.Sum_.Dlink.Dlink
	:members:
	:undoc-members:
	:noindex:
Address
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IPVFour:ADDRess:TYPE

.. code-block:: python

	CONFigure:DATA:CONTrol:IPVFour:ADDRess:TYPE



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.IpvFour_.Address.Address
	:members:
	:undoc-members:
	:noindex:
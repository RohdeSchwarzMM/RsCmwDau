Factory
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:CONFerence:FACTory:DELete

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:CONFerence:FACTory:DELete



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Conference_.Factory.Factory
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.conference.factory.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_Conference_Factory_Add.rst
Certificate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:CERTificate:KEY
	single: CONFigure:DATA:CONTrol:EPDG:CERTificate:ENABle
	single: CONFigure:DATA:CONTrol:EPDG:CERTificate:CERTificate

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:CERTificate:KEY
	CONFigure:DATA:CONTrol:EPDG:CERTificate:ENABle
	CONFigure:DATA:CONTrol:EPDG:CERTificate:CERTificate



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Certificate.Certificate
	:members:
	:undoc-members:
	:noindex:
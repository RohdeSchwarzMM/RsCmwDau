IpvFour
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Dns_.Foreign_.IpvFour.IpvFour
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.dns.foreign.ipvFour.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Dns_Foreign_IpvFour_Primary.rst
	Configure_Data_Control_Dns_Foreign_IpvFour_Secondary.rst
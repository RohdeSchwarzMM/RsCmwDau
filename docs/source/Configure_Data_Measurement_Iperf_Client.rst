Client<Client>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix8
	rc = driver.configure.data.measurement.iperf.client.repcap_client_get()
	driver.configure.data.measurement.iperf.client.repcap_client_set(repcap.Client.Ix1)





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Client.Client
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.iperf.client.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Measurement_Iperf_Client_SbSize.rst
	Configure_Data_Measurement_Iperf_Client_Enable.rst
	Configure_Data_Measurement_Iperf_Client_Protocol.rst
	Configure_Data_Measurement_Iperf_Client_Wsize.rst
	Configure_Data_Measurement_Iperf_Client_Port.rst
	Configure_Data_Measurement_Iperf_Client_IpAddress.rst
	Configure_Data_Measurement_Iperf_Client_Pconnection.rst
	Configure_Data_Measurement_Iperf_Client_Bitrate.rst
	Configure_Data_Measurement_Iperf_Client_Reverse.rst
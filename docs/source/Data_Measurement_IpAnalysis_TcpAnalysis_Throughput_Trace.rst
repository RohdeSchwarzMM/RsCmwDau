Trace
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:TCPanalysis:THRoughput:TRACe

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:TCPanalysis:THRoughput:TRACe



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.TcpAnalysis_.Throughput_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:DATA:CONTrol:SUPL:STATe

.. code-block:: python

	SOURce:DATA:CONTrol:SUPL:STATe



.. autoclass:: RsCmwDau.Implementations.Source_.Data_.Control_.Supl_.State.State
	:members:
	:undoc-members:
	:noindex:
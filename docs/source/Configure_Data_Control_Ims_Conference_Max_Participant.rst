Participant
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:CONFerence:MAX:PARTicipant

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:CONFerence:MAX:PARTicipant



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Conference_.Max_.Participant.Participant
	:members:
	:undoc-members:
	:noindex:
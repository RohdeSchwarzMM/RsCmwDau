Primary
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:DNS:CURRent:IPVSix:PRIMary:ADDRess

.. code-block:: python

	SENSe:DATA:CONTrol:DNS:CURRent:IPVSix:PRIMary:ADDRess



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Dns_.Current_.IpvSix_.Primary.Primary
	:members:
	:undoc-members:
	:noindex:
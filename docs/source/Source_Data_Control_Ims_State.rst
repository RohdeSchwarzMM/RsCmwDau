State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:DATA:CONTrol:IMS<Ims>:STATe

.. code-block:: python

	SOURce:DATA:CONTrol:IMS<Ims>:STATe



.. autoclass:: RsCmwDau.Implementations.Source_.Data_.Control_.Ims_.State.State
	:members:
	:undoc-members:
	:noindex:
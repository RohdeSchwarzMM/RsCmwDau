State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:STATe

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:STATe



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.ipAnalysis.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_IpAnalysis_State_All.rst
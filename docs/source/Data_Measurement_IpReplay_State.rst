State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPReplay:STATe

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPReplay:STATe



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpReplay_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.ipReplay.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_IpReplay_State_All.rst
DtxRecv
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:MTCall:EVS:DTXRecv

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:MTCall:EVS:DTXRecv



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.MtCall_.Evs_.DtxRecv.DtxRecv
	:members:
	:undoc-members:
	:noindex:
EcpFormats
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:CAPPlication:HANDshake:NEGotiated:ECPFormats

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:CAPPlication:HANDshake:NEGotiated:ECPFormats



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.IpcSecurity_.Capplication_.Handshake_.Negotiated_.EcpFormats.EcpFormats
	:members:
	:undoc-members:
	:noindex:
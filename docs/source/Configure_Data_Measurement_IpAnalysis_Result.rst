Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:RESult:IPCS
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:RESult:ALL
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:RESult:TCPanalysis
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:RESult:IPConnect
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:RESult:DPCP
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:RESult:FTTRigger
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:RESult:VOIMs

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:RESult:IPCS
	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:RESult:ALL
	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:RESult:TCPanalysis
	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:RESult:IPConnect
	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:RESult:DPCP
	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:RESult:FTTRigger
	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:RESult:VOIMs



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.IpAnalysis_.Result.Result
	:members:
	:undoc-members:
	:noindex:
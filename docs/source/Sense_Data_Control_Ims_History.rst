History
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IMS<Ims>:HISTory

.. code-block:: python

	SENSe:DATA:CONTrol:IMS<Ims>:HISTory



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.History.History
	:members:
	:undoc-members:
	:noindex:
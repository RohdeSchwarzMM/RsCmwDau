SbSize
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:CLIent<Client>:SBSize

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:CLIent<Client>:SBSize



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Client_.SbSize.SbSize
	:members:
	:undoc-members:
	:noindex:
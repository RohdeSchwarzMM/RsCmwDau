State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:ADELay:STATe

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:ADELay:STATe



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Adelay_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.adelay.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_Adelay_State_All.rst
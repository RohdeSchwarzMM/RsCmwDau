Dpd
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:DPD:ENABle
	single: CONFigure:DATA:CONTrol:EPDG:DPD:INTerval
	single: CONFigure:DATA:CONTrol:EPDG:DPD:TIMeout

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:DPD:ENABle
	CONFigure:DATA:CONTrol:EPDG:DPD:INTerval
	CONFigure:DATA:CONTrol:EPDG:DPD:TIMeout



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Dpd.Dpd
	:members:
	:undoc-members:
	:noindex:
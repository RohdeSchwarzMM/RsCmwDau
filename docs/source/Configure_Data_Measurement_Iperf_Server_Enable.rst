Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:SERVer<Server>:ENABle

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:SERVer<Server>:ENABle



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Server_.Enable.Enable
	:members:
	:undoc-members:
	:noindex:
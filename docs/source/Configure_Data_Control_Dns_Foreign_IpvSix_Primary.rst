Primary
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:DNS:FOReign:IPVSix:PRIMary:UDHCp
	single: CONFigure:DATA:CONTrol:DNS:FOReign:IPVSix:PRIMary:ADDRess

.. code-block:: python

	CONFigure:DATA:CONTrol:DNS:FOReign:IPVSix:PRIMary:UDHCp
	CONFigure:DATA:CONTrol:DNS:FOReign:IPVSix:PRIMary:ADDRess



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Dns_.Foreign_.IpvSix_.Primary.Primary
	:members:
	:undoc-members:
	:noindex:
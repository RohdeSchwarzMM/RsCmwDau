DpApplic
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:DPCP:DPAPplic

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:DPCP:DPAPplic



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.Dpcp_.DpApplic.DpApplic
	:members:
	:undoc-members:
	:noindex:
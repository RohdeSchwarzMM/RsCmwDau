IpvSix
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.IpvSix.IpvSix
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.ipvSix.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_IpvSix_Current.rst
	Sense_Data_Control_IpvSix_Automatic.rst
	Sense_Data_Control_IpvSix_Static.rst
	Sense_Data_Control_IpvSix_Dhcp.rst
	Sense_Data_Control_IpvSix_Manual.rst
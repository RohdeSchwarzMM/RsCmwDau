SbSize
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:NAT<Nat>:SBSize

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:NAT<Nat>:SBSize



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Nat_.SbSize.SbSize
	:members:
	:undoc-members:
	:noindex:
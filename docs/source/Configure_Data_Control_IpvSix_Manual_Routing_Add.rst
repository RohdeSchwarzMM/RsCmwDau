Add
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IPVSix:MANual:ROUTing:ADD

.. code-block:: python

	CONFigure:DATA:CONTrol:IPVSix:MANual:ROUTing:ADD



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.IpvSix_.Manual_.Routing_.Add.Add
	:members:
	:undoc-members:
	:noindex:
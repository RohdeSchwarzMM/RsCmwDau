Test
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:DNS:TEST:DOMain
	single: CONFigure:DATA:CONTrol:DNS:TEST:STARt

.. code-block:: python

	CONFigure:DATA:CONTrol:DNS:TEST:DOMain
	CONFigure:DATA:CONTrol:DNS:TEST:STARt



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Dns_.Test.Test
	:members:
	:undoc-members:
	:noindex:
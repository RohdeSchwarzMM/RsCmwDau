Bw
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:EVS:SEND:BW

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:EVS:SEND:BW



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Evs_.Send_.Bw.Bw
	:members:
	:undoc-members:
	:noindex:
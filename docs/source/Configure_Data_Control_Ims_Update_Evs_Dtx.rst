Dtx
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:EVS:DTX

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:EVS:DTX



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Evs_.Dtx.Dtx
	:members:
	:undoc-members:
	:noindex:
Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:NAT<Nat>:ENABle

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:NAT<Nat>:ENABle



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Nat_.Enable.Enable
	:members:
	:undoc-members:
	:noindex:
Clean
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.IpAnalysis_.IpcSecurity_.PrtScan_.Clean.Clean
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.ipAnalysis.ipcSecurity.prtScan.clean.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Measurement_IpAnalysis_IpcSecurity_PrtScan_Clean_General.rst
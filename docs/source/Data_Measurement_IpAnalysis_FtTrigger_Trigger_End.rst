End
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:FTTRigger:TRIGger:END

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:FTTRigger:TRIGger:END



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.FtTrigger_.Trigger_.End.End
	:members:
	:undoc-members:
	:noindex:
Rates
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:INBand:AMRNb:CODec:RATes

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:INBand:AMRNb:CODec:RATes



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Inband_.AmRnb_.Codec_.Rates.Rates
	:members:
	:undoc-members:
	:noindex:
Sms
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS:SMS:TYPE
	single: CONFigure:DATA:CONTrol:IMS:SMS:TEXT
	single: CONFigure:DATA:CONTrol:IMS:SMS:SEND

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS:SMS:TYPE
	CONFigure:DATA:CONTrol:IMS:SMS:TEXT
	CONFigure:DATA:CONTrol:IMS:SMS:SEND



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Sms.Sms
	:members:
	:undoc-members:
	:noindex:
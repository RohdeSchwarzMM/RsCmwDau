Control
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Source_.Data_.Control.Control
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.data.control.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Data_Control_Udp.rst
	Source_Data_Control_Supl.rst
	Source_Data_Control_State.rst
	Source_Data_Control_Dns.rst
	Source_Data_Control_Ftp.rst
	Source_Data_Control_Http.rst
	Source_Data_Control_Ims.rst
	Source_Data_Control_Epdg.rst
Current
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Dns_.Current.Current
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.dns.current.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_Dns_Current_IpvFour.rst
	Sense_Data_Control_Dns_Current_IpvSix.rst
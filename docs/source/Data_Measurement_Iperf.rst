Iperf
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:DATA:MEASurement<MeasInstance>:IPERf
	single: STOP:DATA:MEASurement<MeasInstance>:IPERf
	single: ABORt:DATA:MEASurement<MeasInstance>:IPERf
	single: READ:DATA:MEASurement<MeasInstance>:IPERf
	single: FETCh:DATA:MEASurement<MeasInstance>:IPERf

.. code-block:: python

	INITiate:DATA:MEASurement<MeasInstance>:IPERf
	STOP:DATA:MEASurement<MeasInstance>:IPERf
	ABORt:DATA:MEASurement<MeasInstance>:IPERf
	READ:DATA:MEASurement<MeasInstance>:IPERf
	FETCh:DATA:MEASurement<MeasInstance>:IPERf



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Iperf.Iperf
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.iperf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_Iperf_State.rst
	Data_Measurement_Iperf_PacketLoss.rst
	Data_Measurement_Iperf_All.rst
	Data_Measurement_Iperf_Server.rst
	Data_Measurement_Iperf_Client.rst
Add
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:QOS:FILTer:ADD

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:QOS:FILTer:ADD



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Qos_.FilterPy_.Add.Add
	:members:
	:undoc-members:
	:noindex:
Ims<Ims>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix2
	rc = driver.source.data.control.ims.repcap_ims_get()
	driver.source.data.control.ims.repcap_ims_set(repcap.Ims.Ix1)





.. autoclass:: RsCmwDau.Implementations.Source_.Data_.Control_.Ims.Ims
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.data.control.ims.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Data_Control_Ims_State.rst
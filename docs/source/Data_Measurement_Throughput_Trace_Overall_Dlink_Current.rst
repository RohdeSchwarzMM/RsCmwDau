Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:DLINk:CURRent
	single: READ:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:DLINk:CURRent

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:DLINk:CURRent
	READ:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:DLINk:CURRent



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Trace_.Overall_.Dlink_.Current.Current
	:members:
	:undoc-members:
	:noindex:
Attributes
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:VIDeo:ATTRibutes

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:VIDeo:ATTRibutes



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Video_.Attributes.Attributes
	:members:
	:undoc-members:
	:noindex:
Authentic
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:AUTHentic:ALGorithm
	single: CONFigure:DATA:CONTrol:EPDG:AUTHentic:IMSI
	single: CONFigure:DATA:CONTrol:EPDG:AUTHentic:RAND
	single: CONFigure:DATA:CONTrol:EPDG:AUTHentic:AMF
	single: CONFigure:DATA:CONTrol:EPDG:AUTHentic:OPC

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:AUTHentic:ALGorithm
	CONFigure:DATA:CONTrol:EPDG:AUTHentic:IMSI
	CONFigure:DATA:CONTrol:EPDG:AUTHentic:RAND
	CONFigure:DATA:CONTrol:EPDG:AUTHentic:AMF
	CONFigure:DATA:CONTrol:EPDG:AUTHentic:OPC



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Authentic.Authentic
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.epdg.authentic.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Epdg_Authentic_Key.rst
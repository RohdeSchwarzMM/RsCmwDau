Test
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:UDP:TEST

.. code-block:: python

	CONFigure:DATA:CONTrol:UDP:TEST



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Udp_.Test.Test
	:members:
	:undoc-members:
	:noindex:
FtTrigger
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.IpAnalysis_.FtTrigger.FtTrigger
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.ipAnalysis.ftTrigger.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Measurement_IpAnalysis_FtTrigger_Trace.rst
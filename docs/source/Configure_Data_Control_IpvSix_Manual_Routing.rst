Routing
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IPVSix:MANual:ROUTing:DELete

.. code-block:: python

	CONFigure:DATA:CONTrol:IPVSix:MANual:ROUTing:DELete



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.IpvSix_.Manual_.Routing.Routing
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ipvSix.manual.routing.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_IpvSix_Manual_Routing_Add.rst
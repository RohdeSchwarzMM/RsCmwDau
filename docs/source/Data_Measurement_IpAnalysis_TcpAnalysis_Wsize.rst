Wsize
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.TcpAnalysis_.Wsize.Wsize
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.ipAnalysis.tcpAnalysis.wsize.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_IpAnalysis_TcpAnalysis_Wsize_Trace.rst
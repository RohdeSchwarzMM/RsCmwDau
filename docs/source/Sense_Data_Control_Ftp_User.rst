User
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:FTP:USER:CATalog

.. code-block:: python

	SENSe:DATA:CONTrol:FTP:USER:CATalog



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ftp_.User.User
	:members:
	:undoc-members:
	:noindex:
Local
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:DNS:LOCal:DELete

.. code-block:: python

	CONFigure:DATA:CONTrol:DNS:LOCal:DELete



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Dns_.Local.Local
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.dns.local.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Dns_Local_Add.rst
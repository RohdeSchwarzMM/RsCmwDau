Throughput
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:MEASurement<MeasInstance>:THRoughput:INTerval

.. code-block:: python

	SENSe:DATA:MEASurement<MeasInstance>:THRoughput:INTerval



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Measurement_.Throughput.Throughput
	:members:
	:undoc-members:
	:noindex:
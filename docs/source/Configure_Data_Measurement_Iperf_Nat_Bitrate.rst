Bitrate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:NAT<Nat>:BITRate

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:NAT<Nat>:BITRate



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Nat_.Bitrate.Bitrate
	:members:
	:undoc-members:
	:noindex:
Primary
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:DNS:FOReign:IPVFour:PRIMary:ADDRess
	single: CONFigure:DATA:CONTrol:DNS:FOReign:IPVFour:PRIMary:UDHCp

.. code-block:: python

	CONFigure:DATA:CONTrol:DNS:FOReign:IPVFour:PRIMary:ADDRess
	CONFigure:DATA:CONTrol:DNS:FOReign:IPVFour:PRIMary:UDHCp



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Dns_.Foreign_.IpvFour_.Primary.Primary
	:members:
	:undoc-members:
	:noindex:
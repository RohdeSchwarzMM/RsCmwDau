Udp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:UDP:CLOSe

.. code-block:: python

	CONFigure:DATA:CONTrol:UDP:CLOSe



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Udp.Udp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.udp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Udp_Test.rst
	Configure_Data_Control_Udp_Bind.rst
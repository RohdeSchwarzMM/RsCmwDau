Cmr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:VOIMs:BITRate:CMR

.. code-block:: python

	SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:VOIMs:BITRate:CMR



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Measurement_.IpAnalysis_.VoIms_.Bitrate_.Cmr.Cmr
	:members:
	:undoc-members:
	:noindex:
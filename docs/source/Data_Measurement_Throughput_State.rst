State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:THRoughput:STATe

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:THRoughput:STATe



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.throughput.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_Throughput_State_All.rst
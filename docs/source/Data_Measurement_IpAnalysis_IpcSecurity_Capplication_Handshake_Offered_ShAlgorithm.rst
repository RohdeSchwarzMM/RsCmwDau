ShAlgorithm
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:CAPPlication:HANDshake:OFFered:SHALgorithm

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:CAPPlication:HANDshake:OFFered:SHALgorithm



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.IpcSecurity_.Capplication_.Handshake_.Offered_.ShAlgorithm.ShAlgorithm
	:members:
	:undoc-members:
	:noindex:
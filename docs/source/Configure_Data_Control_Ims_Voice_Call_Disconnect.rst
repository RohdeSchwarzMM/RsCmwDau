Disconnect
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS:VOICe:CALL:DISConnect

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS:VOICe:CALL:DISConnect



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Voice_.Call_.Disconnect.Disconnect
	:members:
	:undoc-members:
	:noindex:
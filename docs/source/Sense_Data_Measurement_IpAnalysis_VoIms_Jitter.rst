Jitter
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:VOIMs:JITTer

.. code-block:: python

	SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:VOIMs:JITTer



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Measurement_.IpAnalysis_.VoIms_.Jitter.Jitter
	:members:
	:undoc-members:
	:noindex:
Subscriber<Subscriber>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr5
	rc = driver.configure.data.control.ims.subscriber.repcap_subscriber_get()
	driver.configure.data.control.ims.subscriber.repcap_subscriber_set(repcap.Subscriber.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:SUBScriber<Subscriber>:DELete

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:SUBScriber<Subscriber>:DELete



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Subscriber.Subscriber
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.subscriber.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_Subscriber_Impu.rst
	Configure_Data_Control_Ims_Subscriber_ChatQci.rst
	Configure_Data_Control_Ims_Subscriber_PrivateId.rst
	Configure_Data_Control_Ims_Subscriber_Authentication.rst
	Configure_Data_Control_Ims_Subscriber_ResLength.rst
	Configure_Data_Control_Ims_Subscriber_IpSec.rst
	Configure_Data_Control_Ims_Subscriber_PublicUserId.rst
	Configure_Data_Control_Ims_Subscriber_Add.rst
	Configure_Data_Control_Ims_Subscriber_Create.rst
NrCount
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:PING:NRCount

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:PING:NRCount



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Ping_.NrCount.NrCount
	:members:
	:undoc-members:
	:noindex:
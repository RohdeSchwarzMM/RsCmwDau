Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:ADELay:TRACe:TALoopback:CURRent
	single: READ:DATA:MEASurement<MeasInstance>:ADELay:TRACe:TALoopback:CURRent

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:ADELay:TRACe:TALoopback:CURRent
	READ:DATA:MEASurement<MeasInstance>:ADELay:TRACe:TALoopback:CURRent



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Adelay_.Trace_.TaLoopback_.Current.Current
	:members:
	:undoc-members:
	:noindex:
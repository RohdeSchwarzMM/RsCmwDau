Routing
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IPVSix:MANual:ROUTing:CATalog

.. code-block:: python

	SENSe:DATA:CONTrol:IPVSix:MANual:ROUTing:CATalog



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.IpvSix_.Manual_.Routing.Routing
	:members:
	:undoc-members:
	:noindex:
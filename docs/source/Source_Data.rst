Data
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Source_.Data.Data
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.data.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Data_Control.rst
	Source_Data_Measurement.rst
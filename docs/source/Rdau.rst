Rdau
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Rdau.Rdau
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rdau.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Rdau_State.rst
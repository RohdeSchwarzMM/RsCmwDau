Prefixes
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IPVSix:PREFixes:POOL

.. code-block:: python

	CONFigure:DATA:CONTrol:IPVSix:PREFixes:POOL



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.IpvSix_.Prefixes.Prefixes
	:members:
	:undoc-members:
	:noindex:
Prefixes
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IPVSix:DHCP:PREFixes:CATalog

.. code-block:: python

	SENSe:DATA:CONTrol:IPVSix:DHCP:PREFixes:CATalog



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.IpvSix_.Dhcp_.Prefixes.Prefixes
	:members:
	:undoc-members:
	:noindex:
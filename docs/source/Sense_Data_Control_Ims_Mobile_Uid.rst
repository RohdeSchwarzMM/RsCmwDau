Uid
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IMS:MOBile:UID:PRIVate
	single: SENSe:DATA:CONTrol:IMS:MOBile:UID:PUBLic

.. code-block:: python

	SENSe:DATA:CONTrol:IMS:MOBile:UID:PRIVate
	SENSe:DATA:CONTrol:IMS:MOBile:UID:PUBLic



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.Mobile_.Uid.Uid
	:members:
	:undoc-members:
	:noindex:
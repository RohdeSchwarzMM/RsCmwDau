Primary
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:DNS:PRIMary:STYPe

.. code-block:: python

	CONFigure:DATA:CONTrol:DNS:PRIMary:STYPe



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Dns_.Primary.Primary
	:members:
	:undoc-members:
	:noindex:
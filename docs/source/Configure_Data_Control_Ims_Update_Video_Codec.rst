Codec
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:VIDeo:CODec

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:VIDeo:CODec



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Video_.Codec.Codec
	:members:
	:undoc-members:
	:noindex:
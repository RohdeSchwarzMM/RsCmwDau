RepCaps
=========

MeasInstance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_measInstance_set(repcap.MeasInstance.Inst1)
	# Values (4x):
	Inst1 | Inst2 | Inst3 | Inst4

AccPointName
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.AccPointName.Nr1
	# Range:
	Nr1 .. Nr15
	# All values (15x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15

Client
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Client.Ix1
	# Range:
	Ix1 .. Ix8
	# All values (8x):
	Ix1 | Ix2 | Ix3 | Ix4 | Ix5 | Ix6 | Ix7 | Ix8

Codec
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Codec.Ix1
	# Range:
	Ix1 .. Ix10
	# All values (10x):
	Ix1 | Ix2 | Ix3 | Ix4 | Ix5 | Ix6 | Ix7 | Ix8
	Ix9 | Ix10

Dlink
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Dlink.Ix1
	# Values (4x):
	Ix1 | Ix2 | Ix3 | Ix4

Fltr
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Fltr.Ix1
	# Range:
	Ix1 .. Ix15
	# All values (15x):
	Ix1 | Ix2 | Ix3 | Ix4 | Ix5 | Ix6 | Ix7 | Ix8
	Ix9 | Ix10 | Ix11 | Ix12 | Ix13 | Ix14 | Ix15

Impairments
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Impairments.Ix1
	# Range:
	Ix1 .. Ix15
	# All values (15x):
	Ix1 | Ix2 | Ix3 | Ix4 | Ix5 | Ix6 | Ix7 | Ix8
	Ix9 | Ix10 | Ix11 | Ix12 | Ix13 | Ix14 | Ix15

Ims
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Ims.Ix1
	# Values (2x):
	Ix1 | Ix2

Imsi
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Imsi.Ix1
	# Range:
	Ix1 .. Ix10
	# All values (10x):
	Ix1 | Ix2 | Ix3 | Ix4 | Ix5 | Ix6 | Ix7 | Ix8
	Ix9 | Ix10

Nat
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Nat.Ix1
	# Range:
	Ix1 .. Ix8
	# All values (8x):
	Ix1 | Ix2 | Ix3 | Ix4 | Ix5 | Ix6 | Ix7 | Ix8

PcscFnc
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.PcscFnc.Nr1
	# Range:
	Nr1 .. Nr10
	# All values (10x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10

Profile
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Profile.Nr1
	# Range:
	Nr1 .. Nr5
	# All values (5x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5

Server
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Server.Ix1
	# Range:
	Ix1 .. Ix8
	# All values (8x):
	Ix1 | Ix2 | Ix3 | Ix4 | Ix5 | Ix6 | Ix7 | Ix8

Slot
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Slot.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

Subscriber
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Subscriber.Nr1
	# Range:
	Nr1 .. Nr5
	# All values (5x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5

Trace
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Trace.Ix1
	# Range:
	Ix1 .. Ix10
	# All values (10x):
	Ix1 | Ix2 | Ix3 | Ix4 | Ix5 | Ix6 | Ix7 | Ix8
	Ix9 | Ix10

UserId
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.UserId.Ix1
	# Range:
	Ix1 .. Ix10
	# All values (10x):
	Ix1 | Ix2 | Ix3 | Ix4 | Ix5 | Ix6 | Ix7 | Ix8
	Ix9 | Ix10

VirtualSubscriber
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.VirtualSubscriber.Nr1
	# Range:
	Nr1 .. Nr20
	# All values (20x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20


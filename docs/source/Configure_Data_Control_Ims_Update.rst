Update
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update.Update
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.update.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_Update_Rcs.rst
	Configure_Data_Control_Ims_Update_Inband.rst
	Configure_Data_Control_Ims_Update_Call.rst
	Configure_Data_Control_Ims_Update_Evs.rst
	Configure_Data_Control_Ims_Update_AdCodec.rst
	Configure_Data_Control_Ims_Update_Amr.rst
	Configure_Data_Control_Ims_Update_Video.rst
	Configure_Data_Control_Ims_Update_Perform.rst
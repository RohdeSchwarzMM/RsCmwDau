Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:CALL:EVENt

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:CALL:EVENt



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Call_.Event.Event
	:members:
	:undoc-members:
	:noindex:
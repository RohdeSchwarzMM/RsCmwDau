Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:QOS:FILTer<Fltr>:ENABle

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:QOS:FILTer<Fltr>:ENABle



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Qos_.FilterPy_.Enable.Enable
	:members:
	:undoc-members:
	:noindex:
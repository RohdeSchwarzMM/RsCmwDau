Http
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:HTTP:ENConnection
	single: CONFigure:DATA:CONTrol:HTTP:IPVSix

.. code-block:: python

	CONFigure:DATA:CONTrol:HTTP:ENConnection
	CONFigure:DATA:CONTrol:HTTP:IPVSix



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Http.Http
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.http.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Http_Start.rst
Algorithm
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:SUBScriber<Subscriber>:AUTHenticati:ALGorithm

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:SUBScriber<Subscriber>:AUTHenticati:ALGorithm



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Subscriber_.Authentication_.Algorithm.Algorithm
	:members:
	:undoc-members:
	:noindex:
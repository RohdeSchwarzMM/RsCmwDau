Bitrate
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Measurement_.IpAnalysis_.VoIms_.Bitrate.Bitrate
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.measurement.ipAnalysis.voIms.bitrate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Measurement_IpAnalysis_VoIms_Bitrate_Cmr.rst
Rekey
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:IKE:REKey:ENABle
	single: CONFigure:DATA:CONTrol:EPDG:IKE:REKey:TIME

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:IKE:REKey:ENABle
	CONFigure:DATA:CONTrol:EPDG:IKE:REKey:TIME



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Ike_.Rekey.Rekey
	:members:
	:undoc-members:
	:noindex:
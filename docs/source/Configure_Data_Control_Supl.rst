Supl
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:SUPL:TRANsmit

.. code-block:: python

	CONFigure:DATA:CONTrol:SUPL:TRANsmit



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Supl.Supl
	:members:
	:undoc-members:
	:noindex:
IpConnect
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.IpAnalysis_.IpConnect.IpConnect
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.ipAnalysis.ipConnect.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Measurement_IpAnalysis_IpConnect_FilterPy.rst
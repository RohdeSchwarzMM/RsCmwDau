Manual
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.IpvSix_.Manual.Manual
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.ipvSix.manual.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_IpvSix_Manual_Routing.rst
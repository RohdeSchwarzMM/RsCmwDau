Prefix
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IPVSix:MOBile:PREFix:TYPE

.. code-block:: python

	CONFigure:DATA:CONTrol:IPVSix:MOBile:PREFix:TYPE



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.IpvSix_.Mobile_.Prefix.Prefix
	:members:
	:undoc-members:
	:noindex:
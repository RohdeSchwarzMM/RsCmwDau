Voice
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS:VOICe:AUDiorouting
	single: CONFigure:DATA:CONTrol:IMS:VOICe:TYPE
	single: CONFigure:DATA:CONTrol:IMS:VOICe:AMRType
	single: CONFigure:DATA:CONTrol:IMS:VOICe:VCODec
	single: CONFigure:DATA:CONTrol:IMS:VOICe:LOOPback
	single: CONFigure:DATA:CONTrol:IMS:VOICe:PRECondition

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS:VOICe:AUDiorouting
	CONFigure:DATA:CONTrol:IMS:VOICe:TYPE
	CONFigure:DATA:CONTrol:IMS:VOICe:AMRType
	CONFigure:DATA:CONTrol:IMS:VOICe:VCODec
	CONFigure:DATA:CONTrol:IMS:VOICe:LOOPback
	CONFigure:DATA:CONTrol:IMS:VOICe:PRECondition



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Voice.Voice
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.voice.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_Voice_Codec.rst
	Configure_Data_Control_Ims_Voice_MendPoint.rst
	Configure_Data_Control_Ims_Voice_Call.rst
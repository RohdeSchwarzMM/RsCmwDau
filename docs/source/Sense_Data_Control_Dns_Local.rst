Local
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:DNS:LOCal:CATalog

.. code-block:: python

	SENSe:DATA:CONTrol:DNS:LOCal:CATalog



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Dns_.Local.Local
	:members:
	:undoc-members:
	:noindex:
Drate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:QOS:FILTer<Fltr>:DRATe

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:QOS:FILTer<Fltr>:DRATe



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Qos_.FilterPy_.Drate.Drate
	:members:
	:undoc-members:
	:noindex:
PrtScan
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:PRTScan:STATus

.. code-block:: python

	SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:PRTScan:STATus



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Measurement_.IpAnalysis_.IpcSecurity_.PrtScan.PrtScan
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.measurement.ipAnalysis.ipcSecurity.prtScan.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Measurement_IpAnalysis_IpcSecurity_PrtScan_Event.rst
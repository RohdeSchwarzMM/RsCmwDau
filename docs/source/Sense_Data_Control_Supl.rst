Supl
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:SUPL:RECeive
	single: SENSe:DATA:CONTrol:SUPL:IFACe

.. code-block:: python

	SENSe:DATA:CONTrol:SUPL:RECeive
	SENSe:DATA:CONTrol:SUPL:IFACe



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Supl.Supl
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.supl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_Supl_Transmit.rst
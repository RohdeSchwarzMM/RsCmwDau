Actmypes
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:ECConfig:ACTMypes

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:ECConfig:ACTMypes



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.EcConfig_.Actmypes.Actmypes
	:members:
	:undoc-members:
	:noindex:
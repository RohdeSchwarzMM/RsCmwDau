Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:AMR:CODec<Codec>:ENABle

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:AMR:CODec<Codec>:ENABle



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Amr_.Codec_.Enable.Enable
	:members:
	:undoc-members:
	:noindex:
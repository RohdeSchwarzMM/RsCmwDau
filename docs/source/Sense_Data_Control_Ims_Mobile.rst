Mobile<Profile>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr5
	rc = driver.sense.data.control.ims.mobile.repcap_profile_get()
	driver.sense.data.control.ims.mobile.repcap_profile_set(repcap.Profile.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IMS:MOBile:HDOMain
	single: SENSe:DATA:CONTrol:IMS:MOBile:IPADdress

.. code-block:: python

	SENSe:DATA:CONTrol:IMS:MOBile:HDOMain
	SENSe:DATA:CONTrol:IMS:MOBile:IPADdress



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.Mobile.Mobile
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.ims.mobile.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_Ims_Mobile_CipAddress.rst
	Sense_Data_Control_Ims_Mobile_Status.rst
	Sense_Data_Control_Ims_Mobile_Uid.rst
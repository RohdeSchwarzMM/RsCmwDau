TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IMS<Ims>:ECALl:TYPE

.. code-block:: python

	SENSe:DATA:CONTrol:IMS<Ims>:ECALl:TYPE



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.Ecall_.TypePy.TypePy
	:members:
	:undoc-members:
	:noindex:
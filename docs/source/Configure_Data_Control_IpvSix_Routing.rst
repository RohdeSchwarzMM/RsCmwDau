Routing
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IPVSix:ROUTing:TYPE

.. code-block:: python

	CONFigure:DATA:CONTrol:IPVSix:ROUTing:TYPE



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.IpvSix_.Routing.Routing
	:members:
	:undoc-members:
	:noindex:
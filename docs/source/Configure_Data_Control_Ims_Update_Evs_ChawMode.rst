ChawMode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:EVS:CHAWmode

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:EVS:CHAWmode



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Evs_.ChawMode.ChawMode
	:members:
	:undoc-members:
	:noindex:
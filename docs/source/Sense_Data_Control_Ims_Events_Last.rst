Last
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IMS<Ims>:EVENts:LAST

.. code-block:: python

	SENSe:DATA:CONTrol:IMS<Ims>:EVENts:LAST



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.Events_.Last.Last
	:members:
	:undoc-members:
	:noindex:
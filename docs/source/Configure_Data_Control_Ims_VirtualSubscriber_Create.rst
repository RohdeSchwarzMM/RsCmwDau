Create
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub:CREate

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub:CREate



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.Create.Create
	:members:
	:undoc-members:
	:noindex:
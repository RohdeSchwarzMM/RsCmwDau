Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IPVFour:CURRent:SMASk
	single: SENSe:DATA:CONTrol:IPVFour:CURRent:IPADdress
	single: SENSe:DATA:CONTrol:IPVFour:CURRent:GIP

.. code-block:: python

	SENSe:DATA:CONTrol:IPVFour:CURRent:SMASk
	SENSe:DATA:CONTrol:IPVFour:CURRent:IPADdress
	SENSe:DATA:CONTrol:IPVFour:CURRent:GIP



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.IpvFour_.Current.Current
	:members:
	:undoc-members:
	:noindex:
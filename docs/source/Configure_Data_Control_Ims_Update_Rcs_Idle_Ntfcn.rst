Ntfcn
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:RCS:IDLE:NTFCn

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:RCS:IDLE:NTFCn



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Rcs_.Idle_.Ntfcn.Ntfcn
	:members:
	:undoc-members:
	:noindex:
Trace
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:TCPanalysis:WSIZe:TRACe

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:TCPanalysis:WSIZe:TRACe



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.TcpAnalysis_.Wsize_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:
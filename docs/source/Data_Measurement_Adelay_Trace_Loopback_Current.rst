Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:ADELay:TRACe:LOOPback:CURRent
	single: READ:DATA:MEASurement<MeasInstance>:ADELay:TRACe:LOOPback:CURRent

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:ADELay:TRACe:LOOPback:CURRent
	READ:DATA:MEASurement<MeasInstance>:ADELay:TRACe:LOOPback:CURRent



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Adelay_.Trace_.Loopback_.Current.Current
	:members:
	:undoc-members:
	:noindex:
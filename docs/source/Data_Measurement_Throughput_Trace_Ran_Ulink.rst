Ulink<Slot>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.data.measurement.throughput.trace.ran.ulink.repcap_slot_get()
	driver.data.measurement.throughput.trace.ran.ulink.repcap_slot_set(repcap.Slot.Nr1)





.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Trace_.Ran_.Ulink.Ulink
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.throughput.trace.ran.ulink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_Throughput_Trace_Ran_Ulink_Current.rst
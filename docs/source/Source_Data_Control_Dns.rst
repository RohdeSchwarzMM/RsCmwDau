Dns
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Source_.Data_.Control_.Dns.Dns
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.data.control.dns.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Data_Control_Dns_State.rst
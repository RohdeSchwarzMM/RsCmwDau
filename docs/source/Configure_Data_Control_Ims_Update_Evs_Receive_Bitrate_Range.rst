Range
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:EVS:RECeive:BITRate:RANGe

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:EVS:RECeive:BITRate:RANGe



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Evs_.Receive_.Bitrate_.Range.Range
	:members:
	:undoc-members:
	:noindex:
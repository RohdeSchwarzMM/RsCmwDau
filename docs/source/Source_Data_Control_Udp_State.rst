State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:DATA:CONTrol:UDP:STATe

.. code-block:: python

	SOURce:DATA:CONTrol:UDP:STATe



.. autoclass:: RsCmwDau.Implementations.Source_.Data_.Control_.Udp_.State.State
	:members:
	:undoc-members:
	:noindex:
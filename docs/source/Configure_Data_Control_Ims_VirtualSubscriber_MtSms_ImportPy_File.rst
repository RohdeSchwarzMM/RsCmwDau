File
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:MTSMs:IMPort:FILE

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:MTSMs:IMPort:FILE



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.MtSms_.ImportPy_.File.File
	:members:
	:undoc-members:
	:noindex:
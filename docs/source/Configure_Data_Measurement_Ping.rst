Ping
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:PING:TIMeout
	single: CONFigure:DATA:MEASurement<MeasInstance>:PING:DIPaddress
	single: CONFigure:DATA:MEASurement<MeasInstance>:PING:PSIZe
	single: CONFigure:DATA:MEASurement<MeasInstance>:PING:PCOunt
	single: CONFigure:DATA:MEASurement<MeasInstance>:PING:INTerval

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:PING:TIMeout
	CONFigure:DATA:MEASurement<MeasInstance>:PING:DIPaddress
	CONFigure:DATA:MEASurement<MeasInstance>:PING:PSIZe
	CONFigure:DATA:MEASurement<MeasInstance>:PING:PCOunt
	CONFigure:DATA:MEASurement<MeasInstance>:PING:INTerval



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Ping.Ping
	:members:
	:undoc-members:
	:noindex:
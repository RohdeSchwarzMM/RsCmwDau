Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:RAN:DLINk<Dlink>:CURRent
	single: READ:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:RAN:DLINk<Dlink>:CURRent

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:RAN:DLINk<Dlink>:CURRent
	READ:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:RAN:DLINk<Dlink>:CURRent



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Trace_.Ran_.Dlink_.Current.Current
	:members:
	:undoc-members:
	:noindex:
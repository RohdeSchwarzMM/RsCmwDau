Test
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:DNS:TEST:RESults

.. code-block:: python

	SENSe:DATA:CONTrol:DNS:TEST:RESults



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Dns_.Test.Test
	:members:
	:undoc-members:
	:noindex:
Nat<Nat>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix8
	rc = driver.configure.data.measurement.iperf.nat.repcap_nat_get()
	driver.configure.data.measurement.iperf.nat.repcap_nat_set(repcap.Nat.Ix1)





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Nat.Nat
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.iperf.nat.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Measurement_Iperf_Nat_SbSize.rst
	Configure_Data_Measurement_Iperf_Nat_Enable.rst
	Configure_Data_Measurement_Iperf_Nat_Protocol.rst
	Configure_Data_Measurement_Iperf_Nat_Port.rst
	Configure_Data_Measurement_Iperf_Nat_Pconnection.rst
	Configure_Data_Measurement_Iperf_Nat_Bitrate.rst
Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:RAN:ULINk<Slot>:CURRent
	single: FETCh:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:RAN:ULINk<Slot>:CURRent

.. code-block:: python

	READ:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:RAN:ULINk<Slot>:CURRent
	FETCh:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:RAN:ULINk<Slot>:CURRent



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Trace_.Ran_.Ulink_.Current.Current
	:members:
	:undoc-members:
	:noindex:
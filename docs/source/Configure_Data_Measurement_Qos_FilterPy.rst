FilterPy<Fltr>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix15
	rc = driver.configure.data.measurement.qos.filterPy.repcap_fltr_get()
	driver.configure.data.measurement.qos.filterPy.repcap_fltr_set(repcap.Fltr.Ix1)





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Qos_.FilterPy.FilterPy
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.qos.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Measurement_Qos_FilterPy_Remove.rst
	Configure_Data_Measurement_Qos_FilterPy_HopLimit.rst
	Configure_Data_Measurement_Qos_FilterPy_Add.rst
	Configure_Data_Measurement_Qos_FilterPy_Bitrate.rst
	Configure_Data_Measurement_Qos_FilterPy_SrcpRange.rst
	Configure_Data_Measurement_Qos_FilterPy_Protocol.rst
	Configure_Data_Measurement_Qos_FilterPy_TcpAckPrio.rst
	Configure_Data_Measurement_Qos_FilterPy_Enable.rst
	Configure_Data_Measurement_Qos_FilterPy_IpAddress.rst
	Configure_Data_Measurement_Qos_FilterPy_Prange.rst
	Configure_Data_Measurement_Qos_FilterPy_PlRate.rst
	Configure_Data_Measurement_Qos_FilterPy_Jitter.rst
	Configure_Data_Measurement_Qos_FilterPy_JitterDistribution.rst
	Configure_Data_Measurement_Qos_FilterPy_Delay.rst
	Configure_Data_Measurement_Qos_FilterPy_Crate.rst
	Configure_Data_Measurement_Qos_FilterPy_Drate.rst
	Configure_Data_Measurement_Qos_FilterPy_Rrate.rst
Delay
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:QOS:FILTer<Fltr>:DELay

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:QOS:FILTer<Fltr>:DELay



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Qos_.FilterPy_.Delay.Delay
	:members:
	:undoc-members:
	:noindex:
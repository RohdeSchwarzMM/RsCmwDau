DnsRequests
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:DATA:MEASurement<MeasInstance>:DNSRequests
	single: STOP:DATA:MEASurement<MeasInstance>:DNSRequests
	single: ABORt:DATA:MEASurement<MeasInstance>:DNSRequests

.. code-block:: python

	INITiate:DATA:MEASurement<MeasInstance>:DNSRequests
	STOP:DATA:MEASurement<MeasInstance>:DNSRequests
	ABORt:DATA:MEASurement<MeasInstance>:DNSRequests



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.DnsRequests.DnsRequests
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.dnsRequests.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_DnsRequests_State.rst
Range
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:EVS:SEND:BITRate:RANGe

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:EVS:SEND:BITRate:RANGe



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Evs_.Send_.Bitrate_.Range.Range
	:members:
	:undoc-members:
	:noindex:
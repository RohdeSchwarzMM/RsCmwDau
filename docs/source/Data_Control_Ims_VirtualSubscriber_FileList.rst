FileList
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Data_.Control_.Ims_.VirtualSubscriber_.FileList.FileList
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.control.ims.virtualSubscriber.fileList.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Control_Ims_VirtualSubscriber_FileList_Pcap.rst
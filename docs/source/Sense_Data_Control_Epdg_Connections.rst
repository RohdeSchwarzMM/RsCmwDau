Connections
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Epdg_.Connections.Connections
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.epdg.connections.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_Epdg_Connections_Imsi.rst
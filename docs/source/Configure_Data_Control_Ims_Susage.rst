Susage
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:SUSage

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:SUSage



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Susage.Susage
	:members:
	:undoc-members:
	:noindex:
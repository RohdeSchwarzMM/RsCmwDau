Rekey
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:ESP:REKey:ENABle
	single: CONFigure:DATA:CONTrol:EPDG:ESP:REKey:TIME

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:ESP:REKey:ENABle
	CONFigure:DATA:CONTrol:EPDG:ESP:REKey:TIME



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Esp_.Rekey.Rekey
	:members:
	:undoc-members:
	:noindex:
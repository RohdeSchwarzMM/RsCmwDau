Epdg
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Source_.Data_.Control_.Epdg.Epdg
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.data.control.epdg.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Data_Control_Epdg_State.rst
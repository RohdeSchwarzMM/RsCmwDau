Info
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:PRTScan:CLEan:GENeral:INFO

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:PRTScan:CLEan:GENeral:INFO



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.IpAnalysis_.IpcSecurity_.PrtScan_.Clean_.General_.Info.Info
	:members:
	:undoc-members:
	:noindex:
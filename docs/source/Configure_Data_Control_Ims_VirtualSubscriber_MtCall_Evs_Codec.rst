Codec<Codec>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix10
	rc = driver.configure.data.control.ims.virtualSubscriber.mtCall.evs.codec.repcap_codec_get()
	driver.configure.data.control.ims.virtualSubscriber.mtCall.evs.codec.repcap_codec_set(repcap.Codec.Ix1)





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.MtCall_.Evs_.Codec.Codec
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.virtualSubscriber.mtCall.evs.codec.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_VirtualSubscriber_MtCall_Evs_Codec_Enable.rst
Dns
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Dns.Dns
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.dns.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_Dns_Current.rst
	Sense_Data_Control_Dns_Local.rst
	Sense_Data_Control_Dns_Aservices.rst
	Sense_Data_Control_Dns_Test.rst
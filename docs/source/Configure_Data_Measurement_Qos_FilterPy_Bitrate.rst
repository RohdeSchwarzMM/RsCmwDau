Bitrate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:QOS:FILTer<Fltr>:BITRate

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:QOS:FILTer<Fltr>:BITRate



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Qos_.FilterPy_.Bitrate.Bitrate
	:members:
	:undoc-members:
	:noindex:
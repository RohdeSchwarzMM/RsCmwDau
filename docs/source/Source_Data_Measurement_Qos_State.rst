State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:DATA:MEASurement<MeasInstance>:QOS:STATe

.. code-block:: python

	SOURce:DATA:MEASurement<MeasInstance>:QOS:STATe



.. autoclass:: RsCmwDau.Implementations.Source_.Data_.Measurement_.Qos_.State.State
	:members:
	:undoc-members:
	:noindex:
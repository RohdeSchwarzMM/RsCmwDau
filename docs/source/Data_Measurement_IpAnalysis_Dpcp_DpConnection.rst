DpConnection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:DPCP:DPConnection

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:DPCP:DPConnection



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.Dpcp_.DpConnection.DpConnection
	:members:
	:undoc-members:
	:noindex:
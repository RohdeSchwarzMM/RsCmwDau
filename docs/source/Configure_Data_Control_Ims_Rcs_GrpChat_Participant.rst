Participant
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:RCS:GRPChat:PARTicipant:DELete

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:RCS:GRPChat:PARTicipant:DELete



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Rcs_.GrpChat_.Participant.Participant
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.rcs.grpChat.participant.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_Rcs_GrpChat_Participant_Add.rst
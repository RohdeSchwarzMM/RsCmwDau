Start
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:FTTRigger:TRIGger:STARt

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:FTTRigger:TRIGger:STARt



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.FtTrigger_.Trigger_.Start.Start
	:members:
	:undoc-members:
	:noindex:
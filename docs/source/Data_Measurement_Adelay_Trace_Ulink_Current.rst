Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:ADELay:TRACe:ULINk:CURRent
	single: READ:DATA:MEASurement<MeasInstance>:ADELay:TRACe:ULINk:CURRent

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:ADELay:TRACe:ULINk:CURRent
	READ:DATA:MEASurement<MeasInstance>:ADELay:TRACe:ULINk:CURRent



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Adelay_.Trace_.Ulink_.Current.Current
	:members:
	:undoc-members:
	:noindex:
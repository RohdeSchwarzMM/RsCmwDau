Address
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IPVSix:ADDRess:TYPE

.. code-block:: python

	CONFigure:DATA:CONTrol:IPVSix:ADDRess:TYPE



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.IpvSix_.Address.Address
	:members:
	:undoc-members:
	:noindex:
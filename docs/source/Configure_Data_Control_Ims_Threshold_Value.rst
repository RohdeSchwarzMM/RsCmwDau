Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:THReshold:VALue

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:THReshold:VALue



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Threshold_.Value.Value
	:members:
	:undoc-members:
	:noindex:
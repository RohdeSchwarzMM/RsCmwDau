State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:DATA:CONTrol:DNS:STATe

.. code-block:: python

	SOURce:DATA:CONTrol:DNS:STATe



.. autoclass:: RsCmwDau.Implementations.Source_.Data_.Control_.Dns_.State.State
	:members:
	:undoc-members:
	:noindex:
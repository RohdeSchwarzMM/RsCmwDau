Traces<Trace>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix10
	rc = driver.data.measurement.ipAnalysis.ftTrigger.traces.repcap_trace_get()
	driver.data.measurement.ipAnalysis.ftTrigger.traces.repcap_trace_set(repcap.Trace.Ix1)





.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.FtTrigger_.Traces.Traces
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.ipAnalysis.ftTrigger.traces.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_IpAnalysis_FtTrigger_Traces_Fthroughput.rst
Deploy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:DEPLoy:RESult

.. code-block:: python

	SENSe:DATA:CONTrol:DEPLoy:RESult



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Deploy.Deploy
	:members:
	:undoc-members:
	:noindex:
Nimpairments<Impairments>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix15
	rc = driver.configure.data.measurement.nimpairments.repcap_impairments_get()
	driver.configure.data.measurement.nimpairments.repcap_impairments_set(repcap.Impairments.Ix1)





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Nimpairments.Nimpairments
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.nimpairments.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Measurement_Nimpairments_Enable.rst
	Configure_Data_Measurement_Nimpairments_IpAddress.rst
	Configure_Data_Measurement_Nimpairments_Prange.rst
	Configure_Data_Measurement_Nimpairments_PlRate.rst
	Configure_Data_Measurement_Nimpairments_Jitter.rst
	Configure_Data_Measurement_Nimpairments_JitterDistribution.rst
	Configure_Data_Measurement_Nimpairments_Delay.rst
	Configure_Data_Measurement_Nimpairments_Crate.rst
	Configure_Data_Measurement_Nimpairments_Drate.rst
	Configure_Data_Measurement_Nimpairments_Rrate.rst
Secondary
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:DNS:FOReign:IPVFour:SECondary:ADDRess
	single: CONFigure:DATA:CONTrol:DNS:FOReign:IPVFour:SECondary:UDHCp

.. code-block:: python

	CONFigure:DATA:CONTrol:DNS:FOReign:IPVFour:SECondary:ADDRess
	CONFigure:DATA:CONTrol:DNS:FOReign:IPVFour:SECondary:UDHCp



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Dns_.Foreign_.IpvFour_.Secondary.Secondary
	:members:
	:undoc-members:
	:noindex:
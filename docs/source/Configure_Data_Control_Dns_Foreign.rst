Foreign
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:DNS:FOReign:UDHCp

.. code-block:: python

	CONFigure:DATA:CONTrol:DNS:FOReign:UDHCp



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Dns_.Foreign.Foreign
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.dns.foreign.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Dns_Foreign_IpvFour.rst
	Configure_Data_Control_Dns_Foreign_IpvSix.rst
User
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:FTP:USER:ADD
	single: CONFigure:DATA:CONTrol:FTP:USER:DELete

.. code-block:: python

	CONFigure:DATA:CONTrol:FTP:USER:ADD
	CONFigure:DATA:CONTrol:FTP:USER:DELete



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ftp_.User.User
	:members:
	:undoc-members:
	:noindex:
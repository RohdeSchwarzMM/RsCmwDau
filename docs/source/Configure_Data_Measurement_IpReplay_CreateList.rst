CreateList
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPReplay:CREatelist

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPReplay:CREatelist



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.IpReplay_.CreateList.CreateList
	:members:
	:undoc-members:
	:noindex:
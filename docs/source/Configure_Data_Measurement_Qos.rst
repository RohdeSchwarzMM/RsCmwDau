Qos
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:QOS:MODE

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:QOS:MODE



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Qos.Qos
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.qos.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Measurement_Qos_FilterPy.rst
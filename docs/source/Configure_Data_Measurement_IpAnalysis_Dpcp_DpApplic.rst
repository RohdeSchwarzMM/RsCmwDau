DpApplic
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:DPCP:DPAPplic:APP

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:DPCP:DPAPplic:APP



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.IpAnalysis_.Dpcp_.DpApplic.DpApplic
	:members:
	:undoc-members:
	:noindex:
IpvSix
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:PCSCf:IPVSix:TYPE

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:PCSCf:IPVSix:TYPE



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Pcscf_.IpvSix.IpvSix
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.epdg.pcscf.ipvSix.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Epdg_Pcscf_IpvSix_Address.rst
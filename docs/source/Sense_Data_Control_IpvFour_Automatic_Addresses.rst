Addresses
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IPVFour:AUTomatic:ADDResses:CATalog

.. code-block:: python

	SENSe:DATA:CONTrol:IPVFour:AUTomatic:ADDResses:CATalog



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.IpvFour_.Automatic_.Addresses.Addresses
	:members:
	:undoc-members:
	:noindex:
Address
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:ADDRess:IPVFour
	single: CONFigure:DATA:CONTrol:EPDG:ADDRess:IPVSix

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:ADDRess:IPVFour
	CONFigure:DATA:CONTrol:EPDG:ADDRess:IPVSix



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Address.Address
	:members:
	:undoc-members:
	:noindex:
Epdg
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Epdg.Epdg
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.epdg.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_Epdg_Event.rst
	Sense_Data_Control_Epdg_Connections.rst
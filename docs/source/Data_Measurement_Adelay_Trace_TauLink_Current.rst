Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:ADELay:TRACe:TAULink:CURRent
	single: READ:DATA:MEASurement<MeasInstance>:ADELay:TRACe:TAULink:CURRent

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:ADELay:TRACe:TAULink:CURRent
	READ:DATA:MEASurement<MeasInstance>:ADELay:TRACe:TAULink:CURRent



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Adelay_.Trace_.TauLink_.Current.Current
	:members:
	:undoc-members:
	:noindex:
Iperf
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:TYPE
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:TDURation
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:PSIZe
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:STYPe
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:WSIZe
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:PORT
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:LPORt
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:PROTocol
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:IPADdress
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:BITRate
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:PCONnection

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:TYPE
	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:TDURation
	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:PSIZe
	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:STYPe
	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:WSIZe
	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:PORT
	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:LPORt
	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:PROTocol
	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:IPADdress
	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:BITRate
	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:PCONnection



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf.Iperf
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.iperf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Measurement_Iperf_Server.rst
	Configure_Data_Measurement_Iperf_Client.rst
	Configure_Data_Measurement_Iperf_Nat.rst
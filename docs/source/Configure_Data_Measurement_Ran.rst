Ran
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:RAN:CATaloge
	single: CONFigure:DATA:MEASurement<MeasInstance>:RAN

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:RAN:CATaloge
	CONFigure:DATA:MEASurement<MeasInstance>:RAN



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Ran.Ran
	:members:
	:undoc-members:
	:noindex:
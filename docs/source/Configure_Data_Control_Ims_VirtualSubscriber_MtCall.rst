MtCall
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.MtCall.MtCall
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.virtualSubscriber.mtCall.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_VirtualSubscriber_MtCall_Sdp.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtCall_Evs.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtCall_Bearer.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtCall_Destination.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtCall_TypePy.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtCall_SignalingType.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtCall_AdCodec.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtCall_Amr.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtCall_Video.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtCall_Call.rst
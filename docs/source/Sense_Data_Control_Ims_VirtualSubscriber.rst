VirtualSubscriber<VirtualSubscriber>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr20
	rc = driver.sense.data.control.ims.virtualSubscriber.repcap_virtualSubscriber_get()
	driver.sense.data.control.ims.virtualSubscriber.repcap_virtualSubscriber_set(repcap.VirtualSubscriber.Nr1)





.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.VirtualSubscriber.VirtualSubscriber
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.ims.virtualSubscriber.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_Ims_VirtualSubscriber_MtFileTfr.rst
	Sense_Data_Control_Ims_VirtualSubscriber_MtSms.rst
	Sense_Data_Control_Ims_VirtualSubscriber_MtCall.rst
	Sense_Data_Control_Ims_VirtualSubscriber_Catalog.rst
Key
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:AUTHentic:KEY:TYPE
	single: CONFigure:DATA:CONTrol:EPDG:AUTHentic:KEY

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:AUTHentic:KEY:TYPE
	CONFigure:DATA:CONTrol:EPDG:AUTHentic:KEY



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Authentic_.Key.Key
	:members:
	:undoc-members:
	:noindex:
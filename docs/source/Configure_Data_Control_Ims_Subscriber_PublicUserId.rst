PublicUserId<UserId>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix10
	rc = driver.configure.data.control.ims.subscriber.publicUserId.repcap_userId_get()
	driver.configure.data.control.ims.subscriber.publicUserId.repcap_userId_set(repcap.UserId.Ix1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:SUBScriber<Subscriber>:PUBLicuserid<UserId>

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:SUBScriber<Subscriber>:PUBLicuserid<UserId>



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Subscriber_.PublicUserId.PublicUserId
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.subscriber.publicUserId.clone()
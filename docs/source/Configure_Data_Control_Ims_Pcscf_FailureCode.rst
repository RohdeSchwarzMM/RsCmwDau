FailureCode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:PCSCf<PcscFnc>:FAILurecode

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:PCSCf<PcscFnc>:FAILurecode



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Pcscf_.FailureCode.FailureCode
	:members:
	:undoc-members:
	:noindex:
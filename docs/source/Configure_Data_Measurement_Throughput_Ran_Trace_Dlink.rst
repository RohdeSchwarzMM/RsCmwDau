Dlink<Dlink>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix4
	rc = driver.configure.data.measurement.throughput.ran.trace.dlink.repcap_dlink_get()
	driver.configure.data.measurement.throughput.ran.trace.dlink.repcap_dlink_set(repcap.Dlink.Ix1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:THRoughput:RAN:TRACe:DLINk<Dlink>

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:THRoughput:RAN:TRACe:DLINk<Dlink>



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Throughput_.Ran_.Trace_.Dlink.Dlink
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.throughput.ran.trace.dlink.clone()
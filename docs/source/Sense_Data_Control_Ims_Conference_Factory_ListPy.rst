ListPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IMS<Ims>:CONFerence:FACTory:LIST

.. code-block:: python

	SENSe:DATA:CONTrol:IMS<Ims>:CONFerence:FACTory:LIST



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.Conference_.Factory_.ListPy.ListPy
	:members:
	:undoc-members:
	:noindex:
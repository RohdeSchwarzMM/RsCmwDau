Imsi<Imsi>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix10
	rc = driver.configure.data.control.epdg.connections.imsi.repcap_imsi_get()
	driver.configure.data.control.epdg.connections.imsi.repcap_imsi_set(repcap.Imsi.Ix1)





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Connections_.Imsi.Imsi
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.epdg.connections.imsi.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Epdg_Connections_Imsi_Apn.rst
Protocol
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:SERVer<Server>:PROTocol

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:SERVer<Server>:PROTocol



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Server_.Protocol.Protocol
	:members:
	:undoc-members:
	:noindex:
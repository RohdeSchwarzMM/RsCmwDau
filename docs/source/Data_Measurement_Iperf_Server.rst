Server
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:DATA:MEASurement<MeasInstance>:IPERf:SERVer
	single: FETCh:DATA:MEASurement<MeasInstance>:IPERf:SERVer

.. code-block:: python

	READ:DATA:MEASurement<MeasInstance>:IPERf:SERVer
	FETCh:DATA:MEASurement<MeasInstance>:IPERf:SERVer



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Iperf_.Server.Server
	:members:
	:undoc-members:
	:noindex:
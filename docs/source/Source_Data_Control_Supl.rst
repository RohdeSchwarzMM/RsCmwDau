Supl
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:DATA:CONTrol:SUPL:REX

.. code-block:: python

	SOURce:DATA:CONTrol:SUPL:REX



.. autoclass:: RsCmwDau.Implementations.Source_.Data_.Control_.Supl.Supl
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.data.control.supl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Data_Control_Supl_Reliability.rst
	Source_Data_Control_Supl_State.rst
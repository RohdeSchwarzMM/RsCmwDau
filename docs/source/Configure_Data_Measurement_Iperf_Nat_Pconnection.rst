Pconnection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:NAT<Nat>:PCONnection

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:NAT<Nat>:PCONnection



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Nat_.Pconnection.Pconnection
	:members:
	:undoc-members:
	:noindex:
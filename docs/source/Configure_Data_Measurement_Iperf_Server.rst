Server<Server>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix8
	rc = driver.configure.data.measurement.iperf.server.repcap_server_get()
	driver.configure.data.measurement.iperf.server.repcap_server_set(repcap.Server.Ix1)





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Server.Server
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.iperf.server.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Measurement_Iperf_Server_SbSize.rst
	Configure_Data_Measurement_Iperf_Server_Enable.rst
	Configure_Data_Measurement_Iperf_Server_Protocol.rst
	Configure_Data_Measurement_Iperf_Server_Wsize.rst
	Configure_Data_Measurement_Iperf_Server_Port.rst
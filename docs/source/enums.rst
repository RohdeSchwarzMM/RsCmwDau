Enums
=========

AddressModeA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AddressModeA.AUTomatic
	# All values (3x):
	AUTomatic | DHCPv4 | STATic

AddressModeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AddressModeB.ACONf
	# All values (3x):
	ACONf | AUTO | STATic

AddressType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AddressType.IPVFour
	# All values (2x):
	IPVFour | IPVSix

AkaVersion
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AkaVersion.AKA1
	# All values (3x):
	AKA1 | AKA2 | HTTP

AlignMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AlignMode.BANDwidtheff
	# All values (2x):
	BANDwidtheff | OCTetaligned

AmRnbBitrate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.AmRnbBitrate.NOReq
	# Last value:
	value = enums.AmRnbBitrate.R795
	# All values (9x):
	NOReq | R1020 | R1220 | R475 | R515 | R590 | R670 | R740
	R795

AmrType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AmrType.NARRowband
	# All values (2x):
	NARRowband | WIDeband

AmRwbBitRate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.AmRwbBitRate.NOReq
	# Last value:
	value = enums.AmRwbBitRate.RA2385
	# All values (10x):
	NOReq | R1265 | R1425 | R1585 | R1825 | R1985 | R2305 | R660
	R885 | RA2385

ApplicationType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ApplicationType.AUDiodelay
	# Last value:
	value = enums.ApplicationType.THRoughput
	# All values (9x):
	AUDiodelay | DNSReq | IPANalysis | IPERf | IPLogging | IPReplay | OVERview | PING
	THRoughput

AudioInstance
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AudioInstance.INST1
	# All values (2x):
	INST1 | INST2

AudioRouting
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AudioRouting.AUDioboard
	# All values (3x):
	AUDioboard | FORWard | LOOPback

AuthAlgorithm
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AuthAlgorithm.MILenage
	# All values (2x):
	MILenage | XOR

AuthScheme
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AuthScheme.AKA1
	# All values (3x):
	AKA1 | AKA2 | NOAuthentic

AvTypeA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvTypeA.AUDio
	# All values (2x):
	AUDio | VIDeo

AvTypeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvTypeB.AUDio
	# All values (3x):
	AUDio | UNKNow | VIDeo

AvTypeC
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvTypeC.AUDio
	# All values (4x):
	AUDio | EMER | UNK | VIDeo

Bandwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Bandwidth.FB
	# All values (7x):
	FB | NB | NBFB | NBSWb | NBWB | SWB | WB

BehaviourA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BehaviourA.AFTRng
	# All values (7x):
	AFTRng | ANSWer | BEFRng | BUSY | CD | DECLined | NOANswer

BehaviourB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BehaviourB.FAILure
	# All values (4x):
	FAILure | NOACcept | NOANswer | NORMal

Bitrate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Bitrate.R1280
	# Last value:
	value = enums.Bitrate.R960
	# All values (12x):
	R1280 | R132 | R164 | R244 | R320 | R480 | R59 | R640
	R72 | R80 | R96 | R960

BwRange
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BwRange.COMMon
	# All values (2x):
	COMMon | SENDrx

CallState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CallState.CALLing
	# All values (7x):
	CALLing | CERRor | CESTablished | NOACtion | NOResponse | RELeased | RINGing

CallType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CallType.ACK
	# All values (8x):
	ACK | GENeric | GPP | GPP2 | LARGe | PAGer | RCSChat | RCSGrpchat

ChawMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChawMode.DIS
	# All values (7x):
	DIS | FIVE | NP | NUSed | SEVen | THRee | TWO

Cmr
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Cmr.DISable
	# All values (4x):
	DISable | ENABle | NP | PRESent

CodecType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodecType.EVS
	# All values (3x):
	EVS | NARRowband | WIDeband

ConnStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConnStatus.CLOSed
	# All values (2x):
	CLOSed | OPEN

DataType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DataType.AUDio
	# All values (8x):
	AUDio | CALL | FILetransfer | FTLMode | INValid | RCSLmsg | SMS | VIDeo

DauState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DauState.ADJusted
	# All values (7x):
	ADJusted | AUTonomous | COUPled | INValid | OFF | ON | PENDing

DauStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DauStatus.CONN
	# All values (2x):
	CONN | NOTConn

DirectionA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DirectionA.DL
	# All values (3x):
	DL | UL | UNKN

DirectionB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DirectionB.DL
	# All values (3x):
	DL | UL | UNK

DtxRecv
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DtxRecv.DISable
	# All values (3x):
	DISable | ENABle | NP

EcallType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EcallType.AUTO
	# All values (2x):
	AUTO | MANU

EvsBitrate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.EvsBitrate.AW1265
	# Last value:
	value = enums.EvsBitrate.WLO7
	# All values (41x):
	AW1265 | AW1425 | AW1585 | AW1825 | AW1985 | AW2305 | AW66 | AW885
	AWB2385 | NONE | NOReq | P1280 | P132 | P164 | P244 | P320
	P480 | P640 | P960 | PR28 | PR59 | PR72 | PR80 | PR96
	SDP | SHO2 | SHO3 | SHO5 | SHO7 | SLO2 | SLO3 | SLO5
	SLO7 | WHO2 | WHO3 | WHO5 | WHO7 | WLO2 | WLO3 | WLO5
	WLO7

EvsBw
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.EvsBw.DEAC
	# Last value:
	value = enums.EvsBw.WBCA
	# All values (9x):
	DEAC | FB | IO | NB | NOReq | SWB | SWBCa | WB
	WBCA

EvsIoModeCnfg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EvsIoModeCnfg.AMRWb
	# All values (2x):
	AMRWb | EVSamrwb

FileTransferType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FileTransferType.FILetransfer
	# All values (2x):
	FILetransfer | LARGe

FilterConnect
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterConnect.BOTH
	# All values (3x):
	BOTH | CLOSed | OPEN

FilterType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterType.APPL
	# All values (8x):
	APPL | CTRY | DSTP | FLOWid | IPADd | L4PR | L7PRotocol | SRCP

ForceModeEvs
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ForceModeEvs.A1265
	# Last value:
	value = enums.ForceModeEvs.SDP
	# All values (22x):
	A1265 | A1425 | A1585 | A1825 | A1985 | A2305 | A2385 | A660
	A885 | P1280 | P132 | P164 | P244 | P28 | P320 | P480
	P640 | P72 | P80 | P96 | P960 | SDP

ForceModeNb
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ForceModeNb.FIVE
	# Last value:
	value = enums.ForceModeNb.ZERO
	# All values (9x):
	FIVE | FOUR | FREE | ONE | SEVN | SIX | THRE | TWO
	ZERO

ForceModeWb
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ForceModeWb.EIGH
	# Last value:
	value = enums.ForceModeWb.ZERO
	# All values (10x):
	EIGH | FIVE | FOUR | FREE | ONE | SEVN | SIX | THRE
	TWO | ZERO

HfOnly
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HfOnly.BOTH
	# All values (3x):
	BOTH | HEADfull | NP

IdType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IdType.ASND
	# All values (7x):
	ASND | ASNG | FQDN | IPVF | IPVS | KEY | RFC

InfoType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InfoType.ERRor
	# All values (4x):
	ERRor | INFO | NONE | WARNing

IpSecEalgorithm
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpSecEalgorithm.AES
	# All values (4x):
	AES | AUTO | DES | NOC

IpSecIalgorithm
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpSecIalgorithm.AUTO
	# All values (3x):
	AUTO | HMMD | HMSH

IpV6AddLgh
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpV6AddLgh.L16
	# All values (2x):
	L16 | L17

JitterDistrib
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.JitterDistrib.NORMal
	# All values (4x):
	NORMal | PAReto | PNORmal | UNIForm

KeyType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.KeyType.OP
	# All values (2x):
	OP | OPC

Layer
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Layer.APP
	# All values (5x):
	APP | FEATure | L3 | L4 | L7

LoggingType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LoggingType.LANDau
	# All values (5x):
	LANDau | UIPClient | UPIP | UPMulti | UPPP

MediaEndpoint
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MediaEndpoint.AUDioboard
	# All values (4x):
	AUDioboard | FORWard | LOOPback | PCAP

MobileStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MobileStatus.EMERgency
	# All values (5x):
	EMERgency | EXPired | REGistered | TERMinated | UNRegistered

MtSmsEncoding
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MtSmsEncoding.BASE64
	# All values (2x):
	BASE64 | NENCoding

NetworkInterface
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetworkInterface.IP
	# All values (3x):
	IP | LANDau | MULTicast

Origin
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Origin.MO
	# All values (3x):
	MO | MT | UNK

OverhUp
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OverhUp.FULL
	# All values (3x):
	FULL | NOK | OK

PauHeader
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PauHeader.COGE
	# All values (8x):
	COGE | CONF | CONRege | CORE | RECN | RECoge | REGD | REGE

PcapMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PcapMode.CYC
	# All values (2x):
	CYC | SING

PcScfStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PcScfStatus.ERRor
	# All values (6x):
	ERRor | OFF | RUNNing | STARting | STOPping | UNKNown

PrefixType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PrefixType.DHCP
	# All values (2x):
	DHCP | STATic

Protocol
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Protocol.TCP
	# All values (2x):
	TCP | UDP

ProtocolB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ProtocolB.ALL
	# All values (3x):
	ALL | TCP | UDP

QosMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.QosMode.PRIO
	# All values (2x):
	PRIO | SAMeprio

ReceiveStatusA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReceiveStatusA.EMPT
	# All values (3x):
	EMPT | ERR | SUCC

ReceiveStatusB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReceiveStatusB.EMPT
	# All values (3x):
	EMPT | ERRO | SUCC

RegisterType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RegisterType.IANA
	# All values (2x):
	IANA | OID

Repetition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repetition.ENDLess
	# All values (2x):
	ENDLess | ONCE

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

Result
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Result.EMPT
	# All values (4x):
	EMPT | ERR | PEND | SUCC

RoutingType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoutingType.MANual
	# All values (2x):
	MANual | RPRotocols

ServerType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ServerType.FOReign
	# All values (4x):
	FOReign | IAForeign | INTernal | NONE

ServiceTypeA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ServiceTypeA.SERVer
	# All values (2x):
	SERVer | TGENerator

ServiceTypeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ServiceTypeB.BIDirectional
	# All values (3x):
	BIDirectional | CLIent | SERVer

SessionState
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SessionState.BUSY
	# Last value:
	value = enums.SessionState.TERMinated
	# All values (20x):
	BUSY | CANCeled | CREated | DECLined | ESTablished | FILetransfer | HOLD | INITialmedia
	MEDiaupdate | NOK | NONE | OK | PROGgres | RCSTxt | REJected | RELeased
	RESumed | RINGing | SRVCcrelease | TERMinated

SessionUsage
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SessionUsage.OFF
	# All values (3x):
	OFF | ONALways | ONBYue

SignalingType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalingType.EARLymedia
	# All values (7x):
	EARLymedia | NOPRecondit | PRECondit | REQU100 | REQuprecondi | SIMPle | WOTPrec183

SipTimerSel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SipTimerSel.CUSTom
	# All values (3x):
	CUSTom | DEFault | RFC

SmsEncoding
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SmsEncoding.ASCI
	# All values (7x):
	ASCI | BASE64 | GSM7 | GSM8 | IAF5 | NENC | UCS

SmsStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SmsStatus.NONE
	# All values (4x):
	NONE | SCOMpleted | SFAiled | SIPRogress

SmsTypeA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SmsTypeA.OGPP
	# All values (6x):
	OGPP | OGPP2 | OPAGer | TGPP | TGPP2 | TPAGer

SmsTypeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SmsTypeB.TGP2
	# All values (2x):
	TGP2 | TGPP

SourceInt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceInt.EXTernal
	# All values (2x):
	EXTernal | INTernal

StartMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StartMode.EAMRwbio
	# All values (2x):
	EAMRwbio | EPRimary

Testcall
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Testcall.FALSe
	# All values (2x):
	FALSe | TRUE

TestResult
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestResult.FAILed
	# All values (3x):
	FAILed | NONE | SUCCeded

ThroughputType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ThroughputType.OVERall
	# All values (2x):
	OVERall | RAN

TransportSel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TransportSel.CUSTom
	# All values (4x):
	CUSTom | DEFault | TCP | UDP

UpdateCallEvent
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UpdateCallEvent.HOLD
	# All values (2x):
	HOLD | RESume

VideoCodec
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VideoCodec.H263
	# All values (2x):
	H263 | H264

VoicePrecondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VoicePrecondition.SIMPle
	# All values (3x):
	SIMPle | WNPRecondit | WPRecondit

VoimState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VoimState.EST
	# All values (5x):
	EST | HOLD | REL | RING | UNK


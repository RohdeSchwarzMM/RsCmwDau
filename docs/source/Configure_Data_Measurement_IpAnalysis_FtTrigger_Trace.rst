Trace<Trace>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix10
	rc = driver.configure.data.measurement.ipAnalysis.ftTrigger.trace.repcap_trace_get()
	driver.configure.data.measurement.ipAnalysis.ftTrigger.trace.repcap_trace_set(repcap.Trace.Ix1)





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.IpAnalysis_.FtTrigger_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.ipAnalysis.ftTrigger.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Measurement_IpAnalysis_FtTrigger_Trace_TflowId.rst
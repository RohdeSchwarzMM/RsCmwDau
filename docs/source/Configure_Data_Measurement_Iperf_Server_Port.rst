Port
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:SERVer<Server>:PORT

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:SERVer<Server>:PORT



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Server_.Port.Port
	:members:
	:undoc-members:
	:noindex:
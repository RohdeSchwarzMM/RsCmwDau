Evs
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Evs.Evs
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.update.evs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_Update_Evs_Codec.rst
	Configure_Data_Control_Ims_Update_Evs_Common.rst
	Configure_Data_Control_Ims_Update_Evs_Receive.rst
	Configure_Data_Control_Ims_Update_Evs_Send.rst
	Configure_Data_Control_Ims_Update_Evs_Synch.rst
	Configure_Data_Control_Ims_Update_Evs_StartMode.rst
	Configure_Data_Control_Ims_Update_Evs_ChawMode.rst
	Configure_Data_Control_Ims_Update_Evs_Cmr.rst
	Configure_Data_Control_Ims_Update_Evs_DtxRecv.rst
	Configure_Data_Control_Ims_Update_Evs_Dtx.rst
	Configure_Data_Control_Ims_Update_Evs_HfOnly.rst
	Configure_Data_Control_Ims_Update_Evs_BwCommon.rst
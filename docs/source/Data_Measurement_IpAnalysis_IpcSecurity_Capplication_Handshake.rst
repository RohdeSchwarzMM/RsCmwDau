Handshake
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.IpcSecurity_.Capplication_.Handshake.Handshake
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.ipAnalysis.ipcSecurity.capplication.handshake.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_IpAnalysis_IpcSecurity_Capplication_Handshake_Negotiated.rst
	Data_Measurement_IpAnalysis_IpcSecurity_Capplication_Handshake_Offered.rst
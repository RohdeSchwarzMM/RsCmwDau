SessionMode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:SUPPorted:FEATures:SESSionmode

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:SUPPorted:FEATures:SESSionmode



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.Supported_.Features_.SessionMode.SessionMode
	:members:
	:undoc-members:
	:noindex:
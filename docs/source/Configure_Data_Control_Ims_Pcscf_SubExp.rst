SubExp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:PCSCf<PcscFnc>:SUBexp

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:PCSCf<PcscFnc>:SUBexp



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Pcscf_.SubExp.SubExp
	:members:
	:undoc-members:
	:noindex:
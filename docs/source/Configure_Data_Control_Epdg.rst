Epdg
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg.Epdg
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.epdg.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Epdg_Pcscf.rst
	Configure_Data_Control_Epdg_Address.rst
	Configure_Data_Control_Epdg_Id.rst
	Configure_Data_Control_Epdg_Ike.rst
	Configure_Data_Control_Epdg_Esp.rst
	Configure_Data_Control_Epdg_Dpd.rst
	Configure_Data_Control_Epdg_Authentic.rst
	Configure_Data_Control_Epdg_Certificate.rst
	Configure_Data_Control_Epdg_Connections.rst
	Configure_Data_Control_Epdg_Clean.rst
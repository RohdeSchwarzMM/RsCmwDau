Select
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:SELect:APP
	single: CONFigure:DATA:MEASurement<MeasInstance>:SELect:THRoughput

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:SELect:APP
	CONFigure:DATA:MEASurement<MeasInstance>:SELect:THRoughput



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Select.Select
	:members:
	:undoc-members:
	:noindex:
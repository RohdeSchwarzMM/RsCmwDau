Control
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:MTU

.. code-block:: python

	CONFigure:DATA:CONTrol:MTU



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control.Control
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Udp.rst
	Configure_Data_Control_Deploy.rst
	Configure_Data_Control_Ims.rst
	Configure_Data_Control_Supl.rst
	Configure_Data_Control_IpvSix.rst
	Configure_Data_Control_Advanced.rst
	Configure_Data_Control_IpvFour.rst
	Configure_Data_Control_Dns.rst
	Configure_Data_Control_Ftp.rst
	Configure_Data_Control_Http.rst
	Configure_Data_Control_Epdg.rst
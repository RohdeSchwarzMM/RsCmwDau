Measurement
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Source_.Data_.Measurement.Measurement
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.data.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Data_Measurement_Qos.rst
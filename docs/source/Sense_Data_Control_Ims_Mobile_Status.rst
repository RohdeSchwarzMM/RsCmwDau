Status
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IMS<Ims>:MOBile<Profile>:STATus

.. code-block:: python

	SENSe:DATA:CONTrol:IMS<Ims>:MOBile<Profile>:STATus



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.Mobile_.Status.Status
	:members:
	:undoc-members:
	:noindex:
Pcscf
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS:INTern:PCSCf:ATYPe

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS:INTern:PCSCf:ATYPe



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Intern_.Pcscf.Pcscf
	:members:
	:undoc-members:
	:noindex:
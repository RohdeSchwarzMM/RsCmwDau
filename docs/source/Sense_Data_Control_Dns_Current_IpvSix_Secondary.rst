Secondary
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:DNS:CURRent:IPVSix:SECondary:ADDRess

.. code-block:: python

	SENSe:DATA:CONTrol:DNS:CURRent:IPVSix:SECondary:ADDRess



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Dns_.Current_.IpvSix_.Secondary.Secondary
	:members:
	:undoc-members:
	:noindex:
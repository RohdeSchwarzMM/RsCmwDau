Mobile<Profile>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr5
	rc = driver.configure.data.control.ims.mobile.repcap_profile_get()
	driver.configure.data.control.ims.mobile.repcap_profile_set(repcap.Profile.Nr1)





.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Mobile.Mobile
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.mobile.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_Mobile_DeRegister.rst
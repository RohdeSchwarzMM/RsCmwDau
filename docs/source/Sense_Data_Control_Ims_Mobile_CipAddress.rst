CipAddress
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IMS<Ims>:MOBile<Profile>:CIPaddress

.. code-block:: python

	SENSe:DATA:CONTrol:IMS<Ims>:MOBile<Profile>:CIPaddress



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.Mobile_.CipAddress.CipAddress
	:members:
	:undoc-members:
	:noindex:
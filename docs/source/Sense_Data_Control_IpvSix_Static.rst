Static
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.IpvSix_.Static.Static
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.ipvSix.static.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_IpvSix_Static_Prefixes.rst
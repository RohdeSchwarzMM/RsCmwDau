Bitrate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:CLIent<Client>:BITRate

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:CLIent<Client>:BITRate



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Client_.Bitrate.Bitrate
	:members:
	:undoc-members:
	:noindex:
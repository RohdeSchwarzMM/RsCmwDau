Id
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:RELease:CALL:ID

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:RELease:CALL:ID



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Release_.Call_.Id.Id
	:members:
	:undoc-members:
	:noindex:
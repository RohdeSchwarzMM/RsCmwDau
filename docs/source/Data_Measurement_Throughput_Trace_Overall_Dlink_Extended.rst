Extended
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:DLINk:EXTended
	single: FETCh:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:DLINk:EXTended

.. code-block:: python

	READ:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:DLINk:EXTended
	FETCh:DATA:MEASurement<MeasInstance>:THRoughput:TRACe:OVERall:DLINk:EXTended



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Trace_.Overall_.Dlink_.Extended.Extended
	:members:
	:undoc-members:
	:noindex:
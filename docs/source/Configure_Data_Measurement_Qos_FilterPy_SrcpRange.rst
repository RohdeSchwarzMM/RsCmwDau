SrcpRange
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:QOS:FILTer<Fltr>:SRCPrange

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:QOS:FILTer<Fltr>:SRCPrange



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Qos_.FilterPy_.SrcpRange.SrcpRange
	:members:
	:undoc-members:
	:noindex:
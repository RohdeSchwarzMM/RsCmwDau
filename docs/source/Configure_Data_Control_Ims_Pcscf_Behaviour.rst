Behaviour
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:PCSCf<PcscFnc>:BEHaviour

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:PCSCf<PcscFnc>:BEHaviour



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Pcscf_.Behaviour.Behaviour
	:members:
	:undoc-members:
	:noindex:
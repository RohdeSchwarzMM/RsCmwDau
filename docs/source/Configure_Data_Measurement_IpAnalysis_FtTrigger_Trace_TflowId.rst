TflowId
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:FTTRigger:TRACe<Trace>:TFLowid

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPANalysis:FTTRigger:TRACe<Trace>:TFLowid



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.IpAnalysis_.FtTrigger_.Trace_.TflowId.TflowId
	:members:
	:undoc-members:
	:noindex:
Perform
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:PERForm

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:PERForm



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Perform.Perform
	:members:
	:undoc-members:
	:noindex:
Ran
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:THRoughput:RAN:CATaloge
	single: CONFigure:DATA:MEASurement<MeasInstance>:THRoughput:RAN:MCOunt
	single: CONFigure:DATA:MEASurement<MeasInstance>:THRoughput:RAN<Slot>

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:THRoughput:RAN:CATaloge
	CONFigure:DATA:MEASurement<MeasInstance>:THRoughput:RAN:MCOunt
	CONFigure:DATA:MEASurement<MeasInstance>:THRoughput:RAN<Slot>



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Throughput_.Ran.Ran
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.measurement.throughput.ran.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Measurement_Throughput_Ran_Trace.rst
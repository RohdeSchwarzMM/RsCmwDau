ListPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:MTCall:DESTination:LIST

.. code-block:: python

	SENSe:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:MTCall:DESTination:LIST



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims_.VirtualSubscriber_.MtCall_.Destination_.ListPy.ListPy
	:members:
	:undoc-members:
	:noindex:
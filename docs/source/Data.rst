Data
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Data.Data
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Control.rst
	Data_Measurement.rst
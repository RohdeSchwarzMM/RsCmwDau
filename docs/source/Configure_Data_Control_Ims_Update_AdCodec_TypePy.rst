TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:ADCodec:TYPE

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:ADCodec:TYPE



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.AdCodec_.TypePy.TypePy
	:members:
	:undoc-members:
	:noindex:
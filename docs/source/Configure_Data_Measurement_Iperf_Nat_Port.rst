Port
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:NAT<Nat>:PORT

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:NAT<Nat>:PORT



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Nat_.Port.Port
	:members:
	:undoc-members:
	:noindex:
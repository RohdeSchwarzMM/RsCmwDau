Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:PRTScan:EVENt:LOG

.. code-block:: python

	SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:IPCSecurity:PRTScan:EVENt:LOG



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Measurement_.IpAnalysis_.IpcSecurity_.PrtScan_.Event.Event
	:members:
	:undoc-members:
	:noindex:
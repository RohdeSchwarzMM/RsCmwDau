Static
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IPVSix:STATic:ADDRess
	single: CONFigure:DATA:CONTrol:IPVSix:STATic:DROuter

.. code-block:: python

	CONFigure:DATA:CONTrol:IPVSix:STATic:ADDRess
	CONFigure:DATA:CONTrol:IPVSix:STATic:DROuter



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.IpvSix_.Static.Static
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ipvSix.static.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_IpvSix_Static_Prefixes.rst
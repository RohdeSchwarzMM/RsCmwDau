Ims<Ims>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix2
	rc = driver.sense.data.control.ims.repcap_ims_get()
	driver.sense.data.control.ims.repcap_ims_set(repcap.Ims.Ix1)





.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Ims.Ims
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.ims.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_Ims_Ecall.rst
	Sense_Data_Control_Ims_Rcs.rst
	Sense_Data_Control_Ims_Conference.rst
	Sense_Data_Control_Ims_Mobile.rst
	Sense_Data_Control_Ims_Pcscf.rst
	Sense_Data_Control_Ims_Intern.rst
	Sense_Data_Control_Ims_Ginfo.rst
	Sense_Data_Control_Ims_VirtualSubscriber.rst
	Sense_Data_Control_Ims_Events.rst
	Sense_Data_Control_Ims_Subscriber.rst
	Sense_Data_Control_Ims_History.rst
	Sense_Data_Control_Ims_Release.rst
	Sense_Data_Control_Ims_Sms.rst
	Sense_Data_Control_Ims_Voice.rst
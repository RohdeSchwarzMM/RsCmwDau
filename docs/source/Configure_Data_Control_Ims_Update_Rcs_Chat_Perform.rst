Perform
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:RCS:CHAT:PERForm

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:UPDate:RCS:CHAT:PERForm



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.Update_.Rcs_.Chat_.Perform.Perform
	:members:
	:undoc-members:
	:noindex:
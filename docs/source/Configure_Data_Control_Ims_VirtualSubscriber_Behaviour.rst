Behaviour
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:BEHaviour

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:BEHaviour



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.Behaviour.Behaviour
	:members:
	:undoc-members:
	:noindex:
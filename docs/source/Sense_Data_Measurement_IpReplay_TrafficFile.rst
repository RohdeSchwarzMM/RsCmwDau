TrafficFile
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:MEASurement<MeasInstance>:IPReplay:TRAFficfile

.. code-block:: python

	SENSe:DATA:MEASurement<MeasInstance>:IPReplay:TRAFficfile



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Measurement_.IpReplay_.TrafficFile.TrafficFile
	:members:
	:undoc-members:
	:noindex:
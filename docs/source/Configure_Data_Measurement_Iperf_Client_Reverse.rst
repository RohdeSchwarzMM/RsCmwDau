Reverse
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:CLIent<Client>:REVerse

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:CLIent<Client>:REVerse



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Client_.Reverse.Reverse
	:members:
	:undoc-members:
	:noindex:
Control
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control.Control
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.data.control.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Data_Control_Services.rst
	Sense_Data_Control_Udp.rst
	Sense_Data_Control_Deploy.rst
	Sense_Data_Control_Ims.rst
	Sense_Data_Control_Supl.rst
	Sense_Data_Control_Lan.rst
	Sense_Data_Control_IpvFour.rst
	Sense_Data_Control_IpvSix.rst
	Sense_Data_Control_Dns.rst
	Sense_Data_Control_Ftp.rst
	Sense_Data_Control_Http.rst
	Sense_Data_Control_Epdg.rst
IpvFour
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:EPDG:PCSCf:IPVFour:TYPE

.. code-block:: python

	CONFigure:DATA:CONTrol:EPDG:PCSCf:IPVFour:TYPE



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Epdg_.Pcscf_.IpvFour.IpvFour
	:members:
	:undoc-members:
	:noindex:
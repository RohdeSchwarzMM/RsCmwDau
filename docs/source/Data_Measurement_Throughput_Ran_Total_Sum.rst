Sum
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Ran_.Total_.Sum.Sum
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.throughput.ran.total.sum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_Throughput_Ran_Total_Sum_Dlink.rst
	Data_Measurement_Throughput_Ran_Total_Sum_Ulink.rst
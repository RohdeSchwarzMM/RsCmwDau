Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:CONTrol:EPDG:EVENt:LOG

.. code-block:: python

	SENSe:DATA:CONTrol:EPDG:EVENt:LOG



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Control_.Epdg_.Event.Event
	:members:
	:undoc-members:
	:noindex:
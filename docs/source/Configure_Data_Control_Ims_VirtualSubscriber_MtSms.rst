MtSms
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:MTSMs:SEND

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:MTSMs:SEND



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.MtSms.MtSms
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.data.control.ims.virtualSubscriber.mtSms.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Data_Control_Ims_VirtualSubscriber_MtSms_ImportPy.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtSms_Encoding.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtSms_Destination.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtSms_TypePy.rst
	Configure_Data_Control_Ims_VirtualSubscriber_MtSms_Text.rst
File
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:EXPort:FILE:PATH

.. code-block:: python

	SENSe:DATA:MEASurement<MeasInstance>:IPANalysis:EXPort:FILE:PATH



.. autoclass:: RsCmwDau.Implementations.Sense_.Data_.Measurement_.IpAnalysis_.Export_.File.File
	:members:
	:undoc-members:
	:noindex:
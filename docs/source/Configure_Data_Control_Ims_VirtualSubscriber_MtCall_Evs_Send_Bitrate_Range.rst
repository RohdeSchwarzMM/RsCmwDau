Range
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:MTCall:EVS:SEND:BITRate:RANGe

.. code-block:: python

	CONFigure:DATA:CONTrol:IMS<Ims>:VIRTualsub<VirtualSubscriber>:MTCall:EVS:SEND:BITRate:RANGe



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Ims_.VirtualSubscriber_.MtCall_.Evs_.Send_.Bitrate_.Range.Range
	:members:
	:undoc-members:
	:noindex:
Dlink
----------------------------------------





.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Throughput_.Trace_.Overall_.Dlink.Dlink
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.data.measurement.throughput.trace.overall.dlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Data_Measurement_Throughput_Trace_Overall_Dlink_Extended.rst
	Data_Measurement_Throughput_Trace_Overall_Dlink_Current.rst
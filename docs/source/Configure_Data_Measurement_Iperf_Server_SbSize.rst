SbSize
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:MEASurement<MeasInstance>:IPERf:SERVer<Server>:SBSize

.. code-block:: python

	CONFigure:DATA:MEASurement<MeasInstance>:IPERf:SERVer<Server>:SBSize



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Measurement_.Iperf_.Server_.SbSize.SbSize
	:members:
	:undoc-members:
	:noindex:
Dlink
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:ADELay:DLINk
	single: READ:DATA:MEASurement<MeasInstance>:ADELay:DLINk

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:ADELay:DLINk
	READ:DATA:MEASurement<MeasInstance>:ADELay:DLINk



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.Adelay_.Dlink.Dlink
	:members:
	:undoc-members:
	:noindex:
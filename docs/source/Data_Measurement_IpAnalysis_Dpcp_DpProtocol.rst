DpProtocol
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:DPCP:DPPRotocol

.. code-block:: python

	FETCh:DATA:MEASurement<MeasInstance>:IPANalysis:DPCP:DPPRotocol



.. autoclass:: RsCmwDau.Implementations.Data_.Measurement_.IpAnalysis_.Dpcp_.DpProtocol.DpProtocol
	:members:
	:undoc-members:
	:noindex:
Add
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:DATA:CONTrol:DNS:LOCal:ADD

.. code-block:: python

	CONFigure:DATA:CONTrol:DNS:LOCal:ADD



.. autoclass:: RsCmwDau.Implementations.Configure_.Data_.Control_.Dns_.Local_.Add.Add
	:members:
	:undoc-members:
	:noindex: